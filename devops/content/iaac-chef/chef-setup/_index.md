+++
title = "Chef Setup"
date = 2021-04-03T23:16:46+05:30
weight = 26300
chapter = false
pre = "<b>3. </b>"
+++

- Chef Infra Server
    - What is the Purpose of Chef Infra Server?
    - Key Components in Chef Infra Server
    - Install Chef Infra Server
    - Significance of Chef Infra Server Programs
    - Configure Chef Infra Server
    - Understanding Chef Infra Server System Paths
    - Manage Chef Infra Server
- Chef Manage
    - What is the Purpose of Chef Manage?
    - Install Chef Manage 
    - Significance of Chef Manage Programs
    - Configure Chef Manage
    - Understanding Chef Manage System Paths
    - Manage Chef Manage
- Chef Push Jobs Server
    - What is the Purpose of Chef Push Jobs Server?
    - Install Chef Push Jobs Server
    - Significance of Chef Push Jobs Server Programs
    - Configure Chef Push Jobs Server
    - Understanding Chef Push Jobs Server System Paths
    - Manage Chef Push Jobs Server 
- Chef Workstation
    - What is the Purpose of Chef Workstation?
    - Install Chef Workstation
    - Significance of Chef Workstation Programs
    - Configure Chef Workstation
    - Understanding Chef Workstation System Paths
    - Do's and Dont's with Chef Workstation
