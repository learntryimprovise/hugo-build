+++
title = "Chef Component"
weight = 26200
chapter = false
pre = "<b>2. </b>"
+++

- Key Components in Chef
    - Chef Infra Server
    - Chef Infra Client
    - Chef Workstation
    - Chef Development Kit
    - Chef Solo
    - Chef Manage
    - Chef Automate
    - Chef Node
    - Chef Push Jobs Server
    - Chef Push Jobs Client
    - Chef Cookbook
    - Chef Role
    - Chef Supermarket 
- Overview of Chef
    - Architecture of Chef-managed Deployments
    - Chef Infra Server
    - Chef Manage
    - Chef Workstation
    - Chef Node
    - Chef Infra Client
    - Chef Push Jobs Server
    - Chef Push Jobs Client
    - Chef Cookbook
    - Chef vs Ansible
- FAQ on Chef
- Prerequisites for Chef
