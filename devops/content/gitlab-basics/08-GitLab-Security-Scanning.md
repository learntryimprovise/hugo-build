+++
title = "Security Scanning"
draft = false
weight = 8
+++

### GitLab Security Scanning 

GitLab provides several built in security scanners, these can be easily added to your .gitlab-ci.yml files by using the includes -template feature. Read more about the various options below!

https://docs.gitlab.com/ee/user/application_security/#


### GitLab SAST Scanning

Use the following website https://docs.gitlab.com/ee/user/application_security/sast/ above to complete the exercise below to indicate whether the statement about GitLab SAST scanning is true or not.

### Exercise 

[LAB 06](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab6.html)