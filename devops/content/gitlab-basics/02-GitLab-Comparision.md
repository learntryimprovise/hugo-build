+++
title = "Comparison"
draft = false
weight = 2
+++

How is GitLab's DevOps Product Different?  

### GitLab Infomercial

Review the [video](https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FgzYTZhJlHoI%3Flist%3DPLFGfElNsQthYpqXUWUl00DMH6p4obUb4V&display_name=YouTube&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DgzYTZhJlHoI&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2FgzYTZhJlHoI%2Fhqdefault.jpg&key=40cb30655a7f4a46adaaf18efb05db21&type=text%2Fhtml&schema=youtube) to see how GitLab can solve the complicated DevOps toolchain problem!

### What problem does GitLab Solve?

Take a look at a real life [example](https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fplayer.vimeo.com%2Fvideo%2F620114567%3Fh%3Da65b489d54%26app_id%3D122963&dntp=1&display_name=Vimeo&url=https%3A%2F%2Fvimeo.com%2F620114567&image=https%3A%2F%2Fi.vimeocdn.com%2Fvideo%2F1262396295-dfbbe0b977393ba1fdfd67a28a89926beaff275c04be591a9_1280&key=40cb30655a7f4a46adaaf18efb05db21&type=text%2Fhtml&schema=vimeo) of how GitLab resolves problems in broken toolchains.


### GitLab's Most Common Customer Use Cases

In a traditional Sequential DevOps approach, each function is running in silos, using their own tools. They may be locally optimized but there is a lot of hand-off tool friction, as every team hands over to next team.But organizations wanting a way to move faster and more efficiently are adopting the GitLab Concurrent DevOps model. This approach offers a way to get teams to work together at the same time and collaborate in a single pane of glass (so to speak) to design, build, test, deliver, and monitor code changes. The teams must work CONCURRENTLY, not SEQUENTIALLY, on the product or service they will deliver to the customer.
In a traditional Sequential DevOps approach, each function is running in silos, using their own tools. They may be locally optimized but there is a lot of hand-off tool friction, as every team hands over to next team.

![DevOps Done Right](/images/gitlab/devops-done-right.png "DevOps Done Right Icon")

But organizations wanting a way to move faster and more efficiently are adopting the GitLab Concurrent DevOps model. This approach offers a way to get teams to work together at the same time and collaborate in a single pane of glass (so to speak) to design, build, test, deliver, and monitor code changes. The teams must work CONCURRENTLY, not SEQUENTIALLY, on the product or service they will deliver to the customer.

![DevOps Lifecycle](/images/gitlab/devops-lifecycle.png "DevOps Lifecycle Icon")

GitLab is a complete open-source DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate and build software. From idea to production, GitLab helps teams improve cycle time from weeks to minutes, reduce development process costs and decrease time to market while increasing developer productivity.

### Deploying GitLab

There are over 20 ways to deploy GitLab, while we recommend using our Omnibus installer since it is quicker to install and easier to upgrade, we also have steps and setups available for a variety of other methods as well, whatever you need to run GitLab your way! You can view all of our different deployment options [here](https://about.gitlab.com/install/).

### A simpler workflow

Organizations coming to Git from other version control systems frequently find it hard to develop a productive workflow. 

![Simple Workflow](/images/gitlab/simple-workflow.png "Simple Workflow Icon")

GitHub introduced a simpler flow that enables creation of feature-specific branches that are then merged into a master branch. But this approach doesn’t address the realities many organizations have of multiple environments, releases, and integrations. 

GitLab flow solves this challenge by integrating the Git workflow with an issue tracking system and ability to create dedicated branches for environments and releases.

`Environment Branches`
![Environment Branches](/images/gitlab/environment-branches.png "Environment Branches Icon")

`Release Branches`
![Release Branches](/images/gitlab/release-branches.png "Release Branches Icon")


### GitLab Recommended Process

Here is the GitLab FLOW process we recommend for DevOps teams to follow while using GitLab capabilities within a concurrent development lifecycle.

![Recommended Process](/images/gitlab/recommended-process.png "Recommended Process Icon")

Lets cover each of the steps in this process as we move through the Concurrent DevOps lifecycle and let you get hands-on with each step.

But first, let’s take a closer look at the GitLab workflow.

`Step 1- Create an Issue`

All changes within GitLab start with an Issue. 

Issues can allow you, your team, and your collaborators to share and discuss proposals before, and during, their implementation. However, they can be used for a variety of other purposes, customized to your needs and workflow.

Issues are always associated with a specific project, but if you have multiple projects in a group, you can also view all the issues collectively at the group level.

Common use cases include:

1. Discussing the implementation of a new idea
2. Tracking tasks and work status
3. Accepting feature proposals, questions, support requests, or bug reports
4. Elaborating on new code implementations


`Step 2- Create a Merge Request`


Once you have created an Issue and added your changes and/or additions, you will Create a Merge Request to start the CI/CD Process.

Merge requests allow you to visualize and collaborate on the proposed changes to source code that exist as commits on a given Git branch. 

You may hear a merge request referred to as a pull request.


`Step 3- Commit Your Changes`

Once your Merge Request has been submitted, you will Commit your changes, which will initiate the CI pipeline.

This step will repeat and recycle as the review process starts and as changes are needed. 

`Step 4- CI Pipeline Runs`

During this step, the CI pipeline will run and initiate code builds, run automated tests, and deploy the branch to the staging environment. 

If there are errors, conflicts, or other issues, the pipeline will fail and will provide the appropriate error message for the failure for you to research the issue. 

`Step 5- Review Apps`

Review apps provide an automatic live preview of changes made in a feature branch by spinning up a dynamic environment for your merge requests.

They allow designers and product managers to see your changes without needing to check out your branch and run your changes in a sandbox environment.

`Step 6- Peer Review & Discussion`

In this step, you will have peers or stakeholders review your changes in the review app and ensure there are no conflicts or edits that need to be made before your commits are finalized.

`Step 7- Approve Changes`

Once the peer/stakeholder review is complete, a person with Merge rights/permissions will need to approve your changes.

`Step 8- Merge; Issue Closed, CD Pipeline Runs`

Once your merge request is approved, the proposed changes are merged into the Master and your issue is closed.

The Continuous Delivery(CD) pipeline will deploy the changes to the production environment and your changes will be live in the system.  

`Step 9- Monitor`

During this step, you monitor your apps to ensure the changes are having the desired effect. 

Keep in mind- with GitLab, it is easy to roll back a change if there is an issue or if anything surfaces in production that needs further adjusting. 

`Summary`
This is the process we recommend for DevOps teams to follow while using GitLab capabilities within a concurrent development lifecycle.

We will go over each of these individual steps in more detail later as you get some hands on experience in the system. 


### GitLab Workflow Components

Gitlab uses terminology for its components that is a bit different from other systems you may have used. Here is a table showing each of the main GitLab components and what their functionality is known as in other systems you may have previously used.

1. Project
    - Also Known as - Repository
    - Function - The core building block where work is organized, managed, tracked and delivered to help the team to collaborate and plan work in the form of issues.
2. Group
    - Also Known as - Project
    - Function - A collection of projects and/or other groups. They are like folders.
3. Issue
    - Also Known as - Story, Narrative, Ticket
    - Function - An issue is part of a project. It is the fundamental planning object where the team documents the use case in the description, discusses the approach, estimates the size/effort (issue weight), tracks actual time/effort, assigns work, and tracks progress.
4. Epic
    - Also Known as - Initiatives, Themes
    - Function - A collection of related issues across different groups and projects to help organize by theme
5. Merge Request
    - Also Known as - Pull Request
    - Function - The linkage between the issue and the actual code changes. Captures the design, implementation details (code changes), discussions (code reviews), approvals, testing (CI Pipeline), and security scans.
6. Label
    - Also Known as - Tag
    - Function - Used to tag and track work for a project or group and associate issues with different initiatives
7. Board
    - Also Known as - Kanban
    - Function - A visual listing of projects and issues useful for teams to manage their backlog of work, prioritize items, and move issues to the team or specific stage in the project.
8. Milestone
    - Also Known as - Release
    - Function - A sprint or deliverable(s), helping you organize code, issues, and merge requests into a cohesive group
9. Roadmap
    - Function - A visual representation of the various epics for the group

Now that you are familiar with what GitLab is and how it compares to other solutions, continue on to learn about the main components and navigation.