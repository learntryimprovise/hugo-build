+++
title = "Basic Code Creation"
draft = false
weight = 5
+++

### Code Review- Typical Workflow

In order to ensure all code that goes into production is of the highest quality, GitLab has a rigorous review process. Regardless of the size or type of change, every change is reviewed using the flow below. 

![Code Review](/images/gitlab/code-review-workflow.png "Code Review Icon")

Here are the tools the team members use inside of GitLab throughout each step of the workflow. I’ll be showing you how each of these work today.

`New Merge Request`

1. Edit files inline on branch
2. Commit Changes - Squash locally Or one commit per file in GUI
3. Merge Request
4. Resolve merge conflicts

`Assign & Review`

1. Assignments
2. Changes tab to view differences
3. Edit Inline
4. Make comments inline
5. Discussion tab to view comments

`Final Assignment`

Assignments feature in GitLab

`Final Review`

1. Assignments
2. Changes tab to view differences
3. Edit Inline
4. Make comments inline
5. Discussion tab to view comments

`Merge Approved`

Approval feature in GitLab.

`Additional Tools for Code Review` 

1. Wiki - https://docs.gitlab.com/ee/user/project/wiki/
2. Web IDE - https://docs.gitlab.com/ee/user/project/web_ide/#web-ide
3. Snippets - https://docs.gitlab.com/ee/user/snippets.html 

### Exercise 

[LAB 03](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab3.html)