+++
title = "Package and Release"
draft = false
weight = 7
+++

### GitLab's Package Stage

GitLab enables teams to package their applications and dependencies, manage containers, and build artifacts with ease. The private, secure container registry and artifact repositories are built-in and preconfigured out-of-the box to work seamlessly with GitLab source code management and CI/CD pipelines. Ensure DevOps acceleration with automated software pipelines that flow freely without interruption.

Using GitLab's package system allows users to quickly search and utilize build artifacts which enhances reuse throughout the organization. This makes it easier for all teams to collaborate and share best practices to minimize time to market and boost overall efficiency.  

### Packages and Registries

The GitLab Package Registry acts as a private or public registry for a variety of common package managers. You can publish and share packages, which can be easily consumed as a dependency in downstream projects.

https://docs.gitlab.com/ee/user/packages/package_registry/index.html


### Package Registry

Every team needs a place to store their packages and dependencies. GitLab aims to provide a comprehensive solution, integrated into our single application, that supports package management for all commonly used languages and binary formats.

### Container Registry

A container registry* is a secure and private registry for Docker images built-in to GitLab. Creating, pushing, and retrieving images works out of the box with GitLab CI/CD.

### Dependency Proxy

The GitLab dependency proxy* can serve as an intermediary between your local developers and automation and the world of packages that need to be fetched from remote repositories. By adding a security and validation layer to a caching proxy, you can ensure reliability, accuracy, and audit-ability for the packages you depend on.

### Helm Chart Registry

Publish Helm packages in your project’s Package Registry. Then install the packages whenever you need to use them as a dependency.

### Git LFS
Git LFS (Large File Storage) is a Git extension, which reduces the impact of large files in your repository by downloading the relevant versions of them lazily. Specifically, large files are downloaded during the checkout process rather than during cloning or fetching.

### GitLab's Release Stage

GitLab helps automate the release and delivery of applications, shortening the delivery lifecycle, streamlining manual processes, and accelerating team velocity. With zero-touch Continuous Delivery (CD) built right into the pipeline, deployments can be automated to multiple environments like staging and production, and the system just knows what to do without being told - even for more advanced patterns like canary deployments. With feature flags, built-in auditing/traceability, on-demand environments, and GitLab pages for static content delivery, you'll be able to deliver faster and with more confidence than ever before.

### CONTINUOUS DELIVERY

The practice of Continuous Delivery (CD) ensures the delivery of CI validated code to your application by means of a structured deployment pipeline.

Together, CI and CD act to accelerate how quickly your team can deliver results for your customers and stakeholders. CI helps you catch and reduce bugs early in the development cycle, and CD moves verified code to your applications faster.

!["Continous Delivery"](/images/gitlab/continous-delivery.png "Continous Delivery Icon")

### PAGES
GitLab Pages is a feature that allows you to publish static websites directly from a repository in GitLab.

You can use it either for personal or business websites, such as portfolios, documentation, manifestos, and business presentations. You can also attribute any license to your content.

Pages does not support dynamic server-side processing, for instance, as .php and .asp requires. 

See this https://docs.gitlab.com/ee/user/project/pages/ article to learn more about static websites vs dynamic websites.

### REVIEW APPS

GitLab's Review Apps include:
-  Automatic Live Preview
Code, commit, and preview your branch in a live environment. Review Apps automatically spin up dynamic environments for your merge requests.
-  One-click to Collaborate
Designers and product managers won't need to check out your branch and run it in a staging environment. Simply send the team a link and let them click around.
- Fully-Integrated
With GitLab's code review, built-in CI/CD, and Review Apps, you can speed up your development process with one tool for coding, testing, and previewing your changes.
- Deployment Flexibility
Deploy to Kubernetes, Heroku, FTP, and more. You can deploy anywhere that you can script with .gitlab-ci.yml and you have full control to deploy as many different kinds of review apps as your team needs.

### INCREMENTAL ROLLOUT

When you have a new version of your app to deploy in production, you may want to use an incremental rollout to replace just a few pods with the latest code. This will allow you to first check how the app is behaving, and later manually increasing the rollout up to 100%.

### FEATURE FLAGS

Feature flags enable teams to achieve CD by letting them deploy dark features to production as smaller batches for controlled testing, separating feature delivery from customer launch, and removing risk from delivery.
*Available for Premium and Ultimate

### RELEASE ORCHESTRATION

Management and orchestration of releases-as-code built on intelligent notifications, scheduling of delivery and shared resources, blackout periods, relationships, parallelization, and sequencing, as well as support for integrating manual processes and interventions.
*Available for Premium and Ultimate

### RELEASE EVIDENCE

Release Evidence includes features such as deploy-time security controls to ensure only trusted container images are deployed on Kubernetes Engine, and more broadly includes all the assurances and evidence collection that are necessary for you to trust the changes you're delivering.

### SECRETS MANAGEMENT

Vault is a secrets management application offered by HashiCorp. It allows you to store and manage sensitive information such as secret environment variables, encryption keys, and authentication tokens. Vault offers Identity-based Access, which means Vault users can authenticate through several of their preferred cloud providers

[How to Publish a Website with GitLab Pages](https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FTWqh9MtT4Bg%3Ffeature%3Doembed&display_name=YouTube&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DTWqh9MtT4Bg&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2FTWqh9MtT4Bg%2Fhqdefault.jpg&key=40cb30655a7f4a46adaaf18efb05db21&type=text%2Fhtml&schema=youtube)


[Deep Dive on Review Apps](https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FHblSiPFamDI%3Ffeature%3Doembed&display_name=YouTube&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DHblSiPFamDI&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2FHblSiPFamDI%2Fhqdefault.jpg&key=40cb30655a7f4a46adaaf18efb05db21&type=text%2Fhtml&schema=youtube)


### Exercise 

[LAB 05](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab5.html)