+++
title = "GitLab Basics"
weight = 4002
chapter = false
+++

---
### What is GitLab? 

GitLab is a complete open-source DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate and build software. From idea to production, GitLab helps teams improve cycle time from weeks to minutes, reduce development process costs and decrease time to market while increasing developer productivity.

### GitLab Intro

Take a look at the [video](https://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FMqL6BMOySIQ%3Flist%3DPLFGfElNsQthYpqXUWUl00DMH6p4obUb4V&display_name=YouTube&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DMqL6BMOySIQ&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2FMqL6BMOySIQ%2Fhqdefault.jpg&key=40cb30655a7f4a46adaaf18efb05db21&type=text%2Fhtml&schema=youtube) below for a brief introduction to GitLab!

### Self Learning LABs

Individual can use below labs to get handon with GitLab's usage
- [LAB 01](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab1.html)
- [LAB 02](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab2.html)
- [LAB 03](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab3.html)
- [LAB 04](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab4.html)
- [LAB 05](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab5.html)
- [LAB 06](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab6.html)