+++
title = "Terraform Overview"
date = 2021-04-04T13:01:06+05:30
weight = 22100
chapter = false
pre = "<b>1. </b>"
+++

- Introduction to Terraform
    - Infrastructure Management
    - What is Capacity Planning?
    - Major Capacity Planning Tools
    - Understanding Capacity Planning
    - Calculating Percentile
    - What is Provisioning?
    - Major Provisioning Tools
- Need for Automated Provisioning
    - What is Deployment?
    - Major Deployment Tools
- What is Terraform?
- History of Terraform
- Key Components in Terraform
    - Terraform Provider
    - Terraform Configuration
    - Terraform Registry
    - Terraform Cloud
- Overview of Terraform
    - Architecture of Terraformmanaged Infrastructure
    - Terraform Configuration
    - Terraform Provider
    - Terraform Provisioner
    - Terraform Registry

