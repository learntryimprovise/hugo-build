+++
title = "Linux Foundation"
weight = 1200
chapter = false
pre = "<b>2. </b>"
+++

### Introduction to Linux
- What is Operating System?
- What is Linux?
- What is Linux Distro?
- Major Linux Distro's
- History of Linux
- Key Components in Linux
- Kernel
- Terminal
- Shell
- User
- Group
- Partition
- Filesystem
- Process
- Package
- Service
- Network
- GUI
- SSH
- Shell Script