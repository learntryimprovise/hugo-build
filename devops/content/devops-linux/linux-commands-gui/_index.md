+++
title = "Linux Commands GUI"
weight = 2600
chapter = false
pre = "<b>6. </b>"
+++

- Useful Commands
- GUI
    - What is the Purpose of GUI?
    - Major GUIs
- SSH
    - What is the Purpose of SSH?
    - History of SSH
    - SSH vs Telnet
    - How SSH Works?
    - Install & Configure SSH Server
    - Configure SSH Key-based Authentication
    - SSH to Remote Host
    - Do's and Dont's with SSH