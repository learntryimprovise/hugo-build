+++
title = "Linux Permissions"
weight = 2200
chapter = false
pre = "<b>2. </b>"
+++

- Shell 
    - What is the Purpose of Shell? 
    - Major Shells 
    - Manage Shells 
    - What is Environment Variable? 
    - Manage Environment Variables 
- User 
    - What is the Purpose of User? 
    - What is Sudo User? 
    - Manage Users 
- Group 
    - What is the Purpose of Group? 
    - Manage Groups 
