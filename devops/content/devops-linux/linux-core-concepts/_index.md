+++
title = "Linux Core Concepts"
weight = 2100
chapter = false
pre = "<b>1. </b>"
+++

###
- Kernel 
    - What is the Purpose of Kernel? 
    - Types of Kernal 
    - How Kernel Works? 
        - Process Management 
        - Memory Management 
        - File System 
- Manage Kernal
- Terminal 
    - What is the Purpose of Terminal?
    - Major Terminal Emulators 
    - What is TTY? 
    - What is Psuedo-Terminal? 
    - Manage Terminals