### Installing Minikube on Windows

1. checking installed version of minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe version
minikube version: v1.27.1
commit: fe869b5d4da11ba318eb84a3ac00f336411de7ba
```
2. All command line options of minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe
minikube provisions and manages local Kubernetes clusters optimized for development workflows.

Basic Commands:
  start            Starts a local Kubernetes cluster
  status           Gets the status of a local Kubernetes cluster
  stop             Stops a running local Kubernetes cluster
  delete           Deletes a local Kubernetes cluster
  dashboard        Access the Kubernetes dashboard running within the minikube cluster
  pause            pause Kubernetes
  unpause          unpause Kubernetes

Images Commands:
  docker-env       Provides instructions to point your terminal's docker-cli to the Docker Engine inside minikube. (Useful for building docker images directly inside minikube)
  podman-env       Configure environment to use minikube's Podman service
  cache            Manage cache for images
  image            Manage images

Configuration and Management Commands:
  addons           Enable or disable a minikube addon
  config           Modify persistent configuration values
  profile          Get or list the current profiles (clusters)
  update-context   Update kubeconfig in case of an IP or port change

Networking and Connectivity Commands:
  service          Returns a URL to connect to a service
  tunnel           Connect to LoadBalancer services

Advanced Commands:
  mount            Mounts the specified directory into minikube
  ssh              Log into the minikube environment (for debugging)
  kubectl          Run a kubectl binary matching the cluster version
  node             Add, remove, or list additional nodes
  cp               Copy the specified file into minikube

Troubleshooting Commands:
  ssh-key          Retrieve the ssh identity key path of the specified node
  ssh-host         Retrieve the ssh host key of the specified node
  ip               Retrieves the IP address of the specified node
  logs             Returns logs to debug a local Kubernetes cluster
  update-check     Print current and latest version number
  version          Print the version of minikube
  options          Show a list of global command-line options (applies to all commands).

Other Commands:
  completion       Generate command completion for a shell

Use "minikube <command> --help" for more information about a given command.

```
3. starting and checking status of minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe start
* minikube v1.27.1 on Microsoft Windows 11 Pro 10.0.22000 Build 22000
* Automatically selected the virtualbox driver
* Downloading VM boot image ...
* Starting control plane node minikube in cluster minikube
* Downloading Kubernetes v1.25.2 preload ...
* Creating virtualbox VM (CPUs=2, Memory=2200MB, Disk=20000MB) ...
! This VM is having trouble accessing https://registry.k8s.io
* To pull new external images, you may need to configure a proxy: https://minikube.sigs.k8s.io/docs/reference/networking/proxy/
* Preparing Kubernetes v1.25.2 on Docker 20.10.18 ...
  - Generating certificates and keys ...
  - Booting up control plane ...
  - Configuring RBAC rules ...
  - Using image gcr.io/k8s-minikube/storage-provisioner:v5
* Verifying Kubernetes components...
* Enabled addons: storage-provisioner, default-storageclass
* kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
* Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe status
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe profile list
|----------|------------|---------|----------------|------|---------|---------|-------|--------|
| Profile  | VM Driver  | Runtime |       IP       | Port | Version | Status  | Nodes | Active |
|----------|------------|---------|----------------|------|---------|---------|-------|--------|
| minikube | virtualbox | docker  | 192.168.59.100 | 8443 | v1.25.2 | Running |     1 | *      |
|----------|------------|---------|----------------|------|---------|---------|-------|--------|

```
5. Stopping and checking status of minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe stop
* Stopping node "minikube"  ...
* 1 node stopped.

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe status
minikube
type: Control Plane
host: Stopped
kubelet: Stopped
apiserver: Stopped
kubeconfig: Stopped

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe profile list
|----------|------------|---------|----------------|------|---------|---------|-------|--------|
| Profile  | VM Driver  | Runtime |       IP       | Port | Version | Status  | Nodes | Active |
|----------|------------|---------|----------------|------|---------|---------|-------|--------|
| minikube | virtualbox | docker  | 192.168.59.100 | 8443 | v1.25.2 | Stopped |     1 | *      |
|----------|------------|---------|----------------|------|---------|---------|-------|--------|

```
6. Deleting existing default node of minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe delete
* Deleting "minikube" in virtualbox ...
* Removed all traces of the "minikube" cluster.

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe status
* Profile "minikube" not found. Run "minikube profile list" to view all profiles.
  To start a cluster, run: "minikube start"

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe profile list

* Exiting due to MK_USAGE_NO_PROFILE: No minikube profile was found.
* Suggestion:

    You can create one using 'minikube start'.

```
7. starting PODMAN based nodes with specific version of kubernetes minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe start --kubernetes-version=v1.22.2 --driver=podman --profile minipod
* [minipod] minikube v1.27.1 on Microsoft Windows 11 Pro 10.0.22000 Build 22000
* Using the podman (experimental) driver based on user configuration

* Exiting due to PROVIDER_PODMAN_NOT_FOUND: The 'podman' provider was not found: exec: "podman": executable file not found in %PATH%
* Suggestion: Install Podman
* Documentation: https://minikube.sigs.k8s.io/docs/drivers/podman/
```
8. starting DOCKER based nodes with specific version of kubernetes minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe start --nodes=2 --kubernetes-version=v1.23.1 --driver=docker --profile doubledocker
* [doubledocker] minikube v1.27.1 on Microsoft Windows 11 Pro 10.0.22000 Build 22000
* Using the docker driver based on user configuration

* Exiting due to PROVIDER_DOCKER_NOT_FOUND: The 'docker' provider was not found: exec: "docker": executable file not found in %PATH%
* Suggestion: Install Docker
* Documentation: https://minikube.sigs.k8s.io/docs/drivers/docker/

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe start --driver=docker --cpus=6 --memory=8g --kubernetes-version="1.23.2" -p largedock
* [doubledocker] minikube v1.27.1 on Microsoft Windows 11 Pro 10.0.22000 Build 22000
* Using the docker driver based on user configuration

* Exiting due to PROVIDER_DOCKER_NOT_FOUND: The 'docker' provider was not found: exec: "docker": executable file not found in %PATH%
* Suggestion: Install Docker
* Documentation: https://minikube.sigs.k8s.io/docs/drivers/docker/

```
9. starting VIRTUALBOX based nodes with specific version of kubernetes with containerd runtime using minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe start --driver=virtualbox --nodes=2 --disk-size=10g --cpus=2 --memory=2g --kubernetes-version=v1.22.4 --cni=calico --container-runtime=containerd -p multivbox
* [multivbox] minikube v1.27.1 on Microsoft Windows 11 Pro 10.0.22000 Build 22000
* Kubernetes 1.25.2 is now available. If you would like to upgrade, specify: --kubernetes-version=v1.25.2
* Using the virtualbox driver based on existing profile
! You cannot change the memory size for an existing minikube cluster. Please first delete the cluster.
* Starting control plane node multivbox in cluster multivbox
* virtualbox "multivbox" VM is missing, will recreate.
* Creating virtualbox VM (CPUs=2, Memory=4096MB, Disk=10240MB) ...
! This VM is having trouble accessing https://k8s.gcr.io
* To pull new external images, you may need to configure a proxy: https://minikube.sigs.k8s.io/docs/reference/networking/proxy/
* Preparing Kubernetes v1.22.4 on containerd 1.6.8 ...
  - Generating certificates and keys ...
  - Booting up control plane ...
  - Configuring RBAC rules ...
* Configuring Calico (Container Networking Interface) ...
  - Using image gcr.io/k8s-minikube/storage-provisioner:v5
* Verifying Kubernetes components...
* Enabled addons: storage-provisioner, default-storageclass
! The cluster multivbox already exists which means the --nodes parameter will be ignored. Use "minikube node add" to add nodes to an existing cluster.
* kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
* Done! kubectl is now configured to use "multivbox" cluster and "default" namespace by default

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe status -p multivbox
multivbox
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe profile list 
|-----------|------------|------------|----------------|------|---------|---------|-------|--------|
|  Profile  | VM Driver  |  Runtime   |       IP       | Port | Version | Status  | Nodes | Active |
|-----------|------------|------------|----------------|------|---------|---------|-------|--------|
| multivbox | virtualbox | containerd | 192.168.59.102 | 8443 | v1.22.4 | Running |     1 |        |
|-----------|------------|------------|----------------|------|---------|---------|-------|--------|
! Found 2 invalid profile(s) !
*        doubledocker
*        minipod
* You can delete them using the following command(s):
         $ minikube delete -p doubledocker
         $ minikube delete -p minipod

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe delete -p multivbox
* Removing C:\Users\SandeepSubhashPancha\.minikube\machines\multivbox ...
* Removed all traces of the "multivbox" cluster.

```

10. starting VIRTUALBOX based nodes with specific version of kubernetes with cri-o runtime using minikube
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe start --driver=virtualbox -n 2 --container-runtime=cri-o --cni=calico -p minibox
* [minibox] minikube v1.27.1 on Microsoft Windows 11 Pro 10.0.22000 Build 22000
* Using the virtualbox driver based on user configuration
* Starting control plane node minibox in cluster minibox
* Downloading Kubernetes v1.25.2 preload ...
* Creating virtualbox VM (CPUs=2, Memory=2200MB, Disk=20000MB) ...
! This VM is having trouble accessing https://registry.k8s.io
* To pull new external images, you may need to configure a proxy: https://minikube.sigs.k8s.io/docs/reference/networking/proxy/
* Preparing Kubernetes v1.25.2 on CRI-O 1.24.1 ...
  - Generating certificates and keys ...
  - Booting up control plane ...
  - Configuring RBAC rules ...
* Configuring Calico (Container Networking Interface) ...
  - Using image gcr.io/k8s-minikube/storage-provisioner:v5
* Verifying Kubernetes components...
* Enabled addons: storage-provisioner, default-storageclass

* Starting worker node minibox-m02 in cluster minibox
* Creating virtualbox VM (CPUs=2, Memory=2200MB, Disk=20000MB) ...
* Found network options:
  - NO_PROXY=192.168.59.103
  - no_proxy=192.168.59.103
! This VM is having trouble accessing https://registry.k8s.io
* To pull new external images, you may need to configure a proxy: https://minikube.sigs.k8s.io/docs/reference/networking/proxy/
* Preparing Kubernetes v1.25.2 on CRI-O 1.24.1 ...
  - env NO_PROXY=192.168.59.103
* Verifying Kubernetes components...
* kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
* Done! kubectl is now configured to use "minibox" cluster and "default" namespace by default

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe profile list
|---------|------------|---------|----------------|------|---------|---------|-------|--------|
| Profile | VM Driver  | Runtime |       IP       | Port | Version | Status  | Nodes | Active |
|---------|------------|---------|----------------|------|---------|---------|-------|--------|
| minibox | virtualbox | crio    | 192.168.59.103 | 8443 | v1.25.2 | Running |     2 |        |
|---------|------------|---------|----------------|------|---------|---------|-------|--------|

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe status  -p minibox
minibox
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured

minibox-m02
type: Worker
host: Running
kubelet: Running
```
11. kubectl 
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/kubectl.exe version
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", GitTreeState:"clean", BuildDate:"2022-03-16T15:58:47Z", GoVersion:"go1.17.8", Compiler:"gc", Platform:"windows/amd64"}
Server Version: version.Info{Major:"1", Minor:"25", GitVersion:"v1.25.2", GitCommit:"5835544ca568b757a8ecae5c153f317e5736700e", GitTreeState:"clean", BuildDate:"2022-09-21T14:27:13Z", GoVersion:"go1.19.1", Compiler:"gc", Platform:"linux/amd64"}
WARNING: version difference between client (1.23) and server (1.25) exceeds the supported minor version skew of +/-1


AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$ ls -lrt ~/.kube/
total 4
-rw-r--r-- 1 AzureAD+SandeepSubhashPancha 4096 872 Oct 15 15:57 config
drwxr-xr-x 1 AzureAD+SandeepSubhashPancha 4096   0 Oct 15 16:50 cache/

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$ cat ~/.kube/config
apiVersion: v1
clusters:
- cluster:
    certificate-authority: C:\Users\SandeepSubhashPancha\.minikube\ca.crt
    extensions:
    - extension:
        last-update: Sat, 15 Oct 2022 15:57:21 IST
        provider: minikube.sigs.k8s.io
        version: v1.27.1
      name: cluster_info
    server: https://192.168.59.103:8443
  name: minibox
contexts:
- context:
    cluster: minibox
    extensions:
    - extension:
        last-update: Sat, 15 Oct 2022 15:57:21 IST
        provider: minikube.sigs.k8s.io
        version: v1.27.1
      name: context_info
    namespace: default
    user: minibox
  name: minibox
current-context: minibox
kind: Config
preferences: {}
users:
- name: minibox
  user:
    client-certificate: C:\Users\SandeepSubhashPancha\.minikube\profiles\minibox\client.crt
    client-key: C:\Users\SandeepSubhashPancha\.minikube\profiles\minibox\client.key

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/kubectl.exe cluster-info
Kubernetes control plane is running at https://192.168.59.103:8443
CoreDNS is running at https://192.168.59.103:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```
12. minikube addons 
```
AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe addons list
|-----------------------------|--------------------------------|
|         ADDON NAME          |           MAINTAINER           |
|-----------------------------|--------------------------------|
| ambassador                  | 3rd party (Ambassador)         |
| auto-pause                  | Google                         |
| csi-hostpath-driver         | Kubernetes                     |
| dashboard                   | Kubernetes                     |
| default-storageclass        | Kubernetes                     |
| efk                         | 3rd party (Elastic)            |
| freshpod                    | Google                         |
| gcp-auth                    | Google                         |
| gvisor                      | Google                         |
| headlamp                    | 3rd party (kinvolk.io)         |
| helm-tiller                 | 3rd party (Helm)               |
| inaccel                     | 3rd party (InAccel             |
|                             | [info@inaccel.com])            |
| ingress                     | Kubernetes                     |
| ingress-dns                 | Google                         |
| istio                       | 3rd party (Istio)              |
| istio-provisioner           | 3rd party (Istio)              |
| kong                        | 3rd party (Kong HQ)            |
| kubevirt                    | 3rd party (KubeVirt)           |
| logviewer                   | 3rd party (unknown)            |
| metallb                     | 3rd party (MetalLB)            |
| metrics-server              | Kubernetes                     |
| nvidia-driver-installer     | Google                         |
| nvidia-gpu-device-plugin    | 3rd party (Nvidia)             |
| olm                         | 3rd party (Operator Framework) |
| pod-security-policy         | 3rd party (unknown)            |
| portainer                   | 3rd party (Portainer.io)       |
| registry                    | Google                         |
| registry-aliases            | 3rd party (unknown)            |
| registry-creds              | 3rd party (UPMC Enterprises)   |
| storage-provisioner         | Google                         |
| storage-provisioner-gluster | 3rd party (Gluster)            |
| volumesnapshots             | Kubernetes                     |
|-----------------------------|--------------------------------|

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe profile list
|---------|------------|---------|----------------|------|---------|---------|-------|--------|
| Profile | VM Driver  | Runtime |       IP       | Port | Version | Status  | Nodes | Active |
|---------|------------|---------|----------------|------|---------|---------|-------|--------|
| minibox | virtualbox | crio    | 192.168.59.103 | 8443 | v1.25.2 | Running |     2 |        |
|---------|------------|---------|----------------|------|---------|---------|-------|--------|

AzureAD+SandeepSubhashPancha@eigen005 MINGW64 /d/SandeepData/VsCodeRepositories/LTE/learntryimprove/hugo-build/devops/content/docker-kubernetes/code (master)
$  /c/Program\ Files/Kubernetes/Minikube/minikube.exe addons enable metrics-server -p minibox
* metrics-server is an addon maintained by Kubernetes. For any concerns contact minikube on GitHub.
You can view the list of minikube maintainers at: https://github.com/kubernetes/minikube/blob/master/OWNERS
  - Using image k8s.gcr.io/metrics-server/metrics-server:v0.6.1
* The 'metrics-server' addon is enabled
```



**13. Single Node Reference Command History**
``` 
# Start a profile
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe start -p singlenode
# Stop a profile
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe stop -p singlenode
# Delete a profile
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe stop -p singlenode
# Get status of a profile
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe status  -p singlenode
# Get Profile details
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe profile list -p singlenode
# Enable Metrics-serve Addon
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe addons enable metrics-server -p singlenode
# Enable Dashboard Addon
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe addons enable dashboard -p singlenode
# Get Cluster Info
$ /c/Program\ Files/Kubernetes/Minikube/kubectl.exe  cluster-info
# Open Dashboard with live terminal
$ /c/Program\ Files/Kubernetes/Minikube/minikube.exe dashboard -p singlenode
# Start Proxy with live terminal
$ /c/Program\ Files/Kubernetes/Minikube/kubectl.exe proxy
# TOKEN Generation
$ TOKEN=$(/c/Program\ Files/Kubernetes/Minikube/kubectl.exe describe secret -n kube-system $(/c/Program\ Files/Kubernetes/Minikube/kubectl.exe get secrets -n kube-system | grep default | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d '\t' | tr -d " ")
# API SERVER
$ /c/Program\ Files/Kubernetes/Minikube/kubectl.exe config view | grep https | cut -f 2- -d ":" | tr -d " "
$ APISERVER=$(/c/Program\ Files/Kubernetes/Minikube/kubectl.exe config view | grep https | cut -f 2- -d ":" | tr -d " ")
#
$ curl $APISERVER --header "Authorization: Bearer $TOKEN" --insecure
# To get all namespaces 
/c/Program\ Files/Kubernetes/Minikube/kubectl.exe get namespaces

```

