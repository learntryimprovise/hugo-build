+++
title = "Minikube - Installing Local Kubernetes Clusters"
weight = 10007
chapter = false
+++

---
### Introduction
- Minikube is one of the easiest, most flexible and popular methods to run an all-in-one or a multi-node local Kubernetes cluster, isolated by Virtual Machines (VM) or Containers, run directly on our workstations. 
- Minikube is the tool responsible for the installation of Kubernetes components, cluster bootstrapping, and cluster tear-down when no longer needed. 
- It includes additional features aimed to ease the user interaction with the Kubernetes cluster, but nonetheless, it initializes for us a fully functional, non-production, Kubernetes cluster extremely convenient for learning purposes. 
- Minikube can be installed on native macOS, Windows, and many Linux distributions.
- In this chapter, we will explore the requirements to install Minikube locally on our workstation.

---
### What is Minikube?
- Minikube is one of the easiest, most flexible and popular methods to run an all-in-one or a multi-node local Kubernetes cluster directly on our local workstations. It installs and runs on any native OS such as Linux, macOS, or Windows. 
- However, in order to fully take advantage of all the features Minikube has to offer, a Type-2 Hypervisor or a Container Runtime should be installed on the local workstation, to run in conjunction with Minikube. 
- The role of the hypervisor or runtime is to offer an isolated infrastructure for the Minikube Kubernetes cluster components, that is easily reproducible, easy to use and tear down. This isolation of the cluster components from our daily environment ensures that once no longer needed, the Minikube components can be safely removed leaving behind no configuration changes to our workstation, thus no traces of their existence. 
- This does not mean, however, that we are responsible for the provisioning of any VMs or containers with guest operating systems with the help of the hypervisor or container runtime. Minikube includes the necessary adapters to interact directly with the isolation software of choice to build all its infrastructure as long as the Type-2 Hypervisor or Container Runtime is installed on our workstation.
- Minikube is built on the capabilities of the **libmachine** library originally designed by Docker to build Virtual Machine container hosts on any physical infrastructure. 
- In time Minikube became very flexible, supporting several hypervisors and container runtimes, depending on the host workstation's native OS.
- For those who feel more adventurous, Minikube can be installed without an isolation software, on bare-metal, which may result in permanent configuration changes to the host OS. 
- To prevent such permanent configuration changes, a second form of isolation can be achieved by installing Minikube inside a Virtual Machine provisioned with a Type-2 Hypervisor of choice, and a desktop guest OS of choice (with enabled GUI). 
- As a result, when installed inside a VM, Minikube will end up making configuration changes to the guest environment, still isolated from the host workstation. Therefore, now we have two distinct methods to isolate the Minikube environment from our host workstation. 
- The isolation software can be specified by the user with the --driver option, otherwise Minikube will try to find a preferred method for the host OS of the workstation.
- Once decided on the isolation method, the next step is to determine the required number of Kubernetes cluster nodes, and their sizes in terms of CPU, memory, and disk space. Minikube invokes the hypervisor of choice to provision the VM(s) which will host the Kubernetes cluster node(s), or the runtime of choice to run container(s) that host the cluster node(s). 
- Keep in mind that Minikube now supports all-in-one single-node and multi-node clusters. 
- Regardless of the isolation method and the expected cluster and node sizes, a local Minikube Kubernetes cluster will ultimately be impacted and/or limited by the physical resources of the host workstation. 
- We have to be mindful of the needs of the host OS and any utilities it may be running, then the needs of the hypervisor or the container runtime, and finally the remaining resources that can be allocated to our Kubernetes cluster. 
- For a learning environment the recommendations are that a Kubernetes node has 2 CPU cores (or virtual CPUs) at a minimum, at least 2 GB of RAM memory (with 4 - 6 GB of RAM recommended for optimal usage), and 20+ GB of disk storage space. 
- When migrating towards a larger, more dynamic, production grade cluster, these resource values should be adjusted accordingly. 
- The Kubernetes nodes are expected to access the internet as well, for software updates, container image downloads, and for client accessibility.
- Following the node(s)' provisioning phase, Minikube invokes kubeadm, to bootstrap the Kubernetes cluster components inside the previously provisioned node(s). 
- We need to ensure that we have the necessary hardware and software required by Minikube to build our environment.

---
### Requirements for Running Minikube
Below we outline the requirements to run Minikube on our local workstation:
- VT-x/AMD-v virtualization must be enabled on the local workstation, and/or verify if it is supported.
- **kubectl ** - kubectl is a binary used to access and manage any Kubernetes cluster. It is installed through Minikube and accessed through the minikube command, or it can be installed separately and run as a standalone tool. We will explore kubectl installation and usage in future chapters.
- Type-2 Hypervisor or Container Runtime
- Without a specified driver, Minikube will try to find an installed hypervisor or a runtime, in the following order of preference (on a Linux host): docker, kvm2, podman, vmware, and virtualbox. If multiple isolation software installations are found, such as docker and virtualbox, Minikube will pick docker over virtualbox if no desired driver is specified by the user. Hypervisors and Container Runtimes supported by various native workstation OSes:
    - On Linux VirtualBox and KVM2 hypervisors, or Docker and Podman runtimes
    - On macOS VirtualBox, HyperKit, VMware Fusion, and Parallels hypervisors, or Docker runtime
    - On Windows VirtualBox, Hyper-V, and VMware Workstation hypervisors, or Docker runtime.
- **NOTE**: Minikube supports a **--driver=none** (on Linux) option that runs the Kubernetes components bare-metal, directly on the host OS and not inside a VM. With this option a Docker installation is required and a Linux OS on the local workstation, but no hypervisor installation. This driver is recommended for advanced users.
- Internet connection on the first Minikube run - to download packages, dependencies, updates and pull images needed to initialize the Minikube Kubernetes cluster components. Subsequent runs will require an internet connection only when new container images need to be pulled from a public container registry or when deployed containerized applications need it for client accessibility. Once a container image has been pulled it can be reused from the local container runtime image cache without an internet connection.
- In this chapter, we use one of the most robust and stable isolation methods as a driver, the VirtualBox hypervisor, to provision the VM(s) which host the components of the Kubernetes cluster. While no longer the preferred driver due to slower startup times when compared with other methods, it is still one of the most stable drivers for Minikube.

---
### Installing Minikube on Linux
- Let's learn how to install the latest Minikube release on Ubuntu Linux 20.04 LTS with VirtualBox v6.1 specifically.
- **NOTE**: For other Linux OS distributions or releases, VirtualBox and Minikube versions, the installation steps may vary! Check the Minikube installation!
- Verify the virtualization support on your Linux OS in a terminal (a non-empty output indicates supported virtualization):
```
$ grep -E --color 'vmx|svm' /proc/cpuinfo
```
- Install the [VirtualBox](https://www.virtualbox.org/wiki/Linux_Downloads) hypervisor. In a terminal run the following commands to add the recommended source repository for local OS, download and register the public key, update and install:
```
$ sudo bash -c 'echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian eoan contrib" >> /etc/apt/sources.list'

$ wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -

$ sudo apt update

$ sudo apt install -y virtualbox-6.1
```
- Install Minikube. We can download and install in a terminal the latest release or a specific release from the Minikube release page:
```
$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb

$ sudo dpkg -i minikube_latest_amd64.deb
```
- **NOTE**: Replacing /latest/ with a particular version, such as /v1.24.0/ will download that specified Minikube version.
- Start Minikube. In a terminal we can start Minikube with the minikube start command, that bootstraps a single-node cluster with the latest stable Kubernetes version release. For a specific Kubernetes version the --kubernetes-version option can be used as such minikube start --kubernetes-version v1.22.0 (where latest is default and acceptable version value, and stable is also acceptable). More advanced start options will be explored later in this chapter:
```
$ minikube start

😄  minikube v1.25.2 on Ubuntu 20.04
✨  Automatically selected the virtualbox driver
💿  Downloading VM boot image ...
    > minikube-v1.25.2.iso.sha256: 65 B / 65 B [-------------] 100.00% ? p/s 0s
    > minikube-v1.25.2.iso: 237.06 MiB / 237.06 MiB [] 100.00% 9.11 MiB p/s 26s
👍  Starting control plane node minikube in cluster minikube
💾  Downloading Kubernetes v1.23.3 preload ...
    > preloaded-images-k8s-v17-v1...: 505.68 MiB / 505.68 MiB  100.00% 8.52 MiB
🔥  Creating virtualbox VM (CPUs=2, Memory=6000MB, Disk=20000MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
💡  kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```
- **NOTE**: An error message that reads "Unable to pick a default driver..." means that Minikube was not able to locate any one of the supported hypervisors or runtimes. The recommendation is to install or re-install a desired isolation tool, and ensuring its executable is found in the default PATH of your OS distribution. 
- Check the status. With the minikube status command, we display the status of the Minikube cluster:
```
$ minikube status

minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```
- Stop Minikube. With the minikube stop command, we can stop Minikube:
```
$ minikube stop

✋  Stopping node "minikube"  ...
🛑  1 node stopped.
```

---
### Getting Started with Minikube on Linux (Demo)
<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="path/to/poster_image.png">
    <source src="../images-docker-kubernetes/ch06-vid01-minikube-install-run-en.mp4" type="video/mp4">
    <!-- <source src="path/to/video.ogg" type="video/ogg"> -->
    <!-- <source src="path/to/video.webm" type="video/webm"> -->
  </video>
</figure>
<!-- blank line -->


---
### Installing Minikube on macOS
- Let's learn how to install the latest Minikube release on macOS with VirtualBox v6.1 specifically.
- **NOTE**: For other VirtualBox and Minikube versions the installation steps may vary! Check the Minikube installation!
- Verify the virtualization support on your macOS in a terminal (VMX in the output indicates enabled virtualization):
```
$ sysctl -a | grep -E --color 'machdep.cpu.features|VMX'
```
- Install the VirtualBox hypervisor for 'OS X hosts'. Download and install the .dmg package.
- Install Minikube. We can download and install in a terminal the latest release or a specific release from the Minikube release page:
```
$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64

$ sudo install minikube-darwin-amd64 /usr/local/bin/minikube
```
- **NOTE**: Replacing /latest/ with a particular version, such as /v1.24.0/ will download that specified version.
- Start Minikube. We can start Minikube with the minikube start command, that bootstraps a single-node cluster with the latest stable Kubernetes version release. For a specific Kubernetes version the --kubernetes-version option can be used as such minikube start --kubernetes-version v1.22.0 (where latest is default and acceptable version value, and stable is also acceptable). More advanced start options will be explored later in this chapter:
```
$ minikube start

😄 minikube v1.25.2 on Darwin 12.3
✨ Automatically selected the virtualbox driver
💿 Downloading VM boot image ...
👍 Starting control plane node minikube in cluster minikube
💾 Downloading Kubernetes v1.23.3 preload ...
🔥 Creating virtualbox VM (CPUs=2, Memory=6000MB, Disk=20000MB) ...
🐳 Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
🔎 Verifying Kubernetes components...
🌟 Enabled addons: default-storageclass, storage-provisioner
💡 kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
🏄 Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```
- **NOTE**: An error message that reads "Unable to pick a default driver..." means that Minikube was not able to locate any one of the supported hypervisors or runtimes. The recommendation is to re-install a desired isolation tool, and ensuring its executable is found in the default PATH of your OS.
- Check the status. With the minikube status command, we display the status of the Minikube cluster:
```
$ minikube status

minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured 
```
- Stop Minikube. With the minikube stop command, we can stop Minikube:
```
$ minikube stop

✋  Stopping node "minikube"  ...
🛑  1 nodes stopped.
```

---
### Installing Minikube on Windows
- Let's learn how to install the latest Minikube release on Windows 10 with VirtualBox v6.1 specifically.
- **NOTE**: For other OS, VirtualBox, and Minikube versions, the installation steps may vary! Check the Minikube installation!
- Verify the virtualization support on your Windows system (multiple output lines ending with 'Yes' indicate supported virtualization):
```
PS C:\WINDOWS\system32> systeminfo
```
- Install the [VirtualBox](https://www.virtualbox.org/wiki/Downloads) hypervisor for 'Windows hosts'. Download and install the .exe package.
- **NOTE**: You may need to disable Hyper-V on your Windows host (if previously installed and used) while running VirtualBox.
- Install Minikube. We can download the latest release or a specific release from the Minikube release page. Once downloaded, we need to make sure it is added to our PATH. There are multiple packages available to download for Windows, found under a Minikube release. However, downloading and installing the .exe will automatically add the executable to the PATH. Let's download and install the latest  [minikube-installer.exe](https://github.com/kubernetes/minikube/releases/latest/download/minikube-installer.exe) package.
- Start Minikube. We can start Minikube using the `minikube start` command, that bootstraps a single-node cluster with the latest stable Kubernetes version release. For a specific Kubernetes version the `--kubernetes-version` option can be used as such `minikube start --kubernetes-version v1.22.0` (where latest is default and acceptable version value, and stable is also acceptable). More advanced start options will be explored later in this chapter. Open the PowerShell using the Run as Administrator option and execute the following command:
```
PS C:\WINDOWS\system32> minikube start

😄  minikube v1.25.2 on Windows 10
✨  Automatically selected the virtualbox driver
💿  Downloading VM boot image ...
👍  Starting control plane node minikube in cluster minikube
💾  Downloading Kubernetes v1.23.3 preload ...
🔥  Creating virtualbox VM (CPUs=2, Memory=6000MB, Disk=20000MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
🔎  Verifying Kubernetes components...
🌟  Enabled addons: default-storageclass, storage-provisioner
💡  kubectl not found. If you need it, try: 'minikube kubectl -- get pods -A'
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```
- **NOTE**: An error message that reads "Unable to pick a default driver..." means that Minikube was not able to locate any one of the supported hypervisors or runtimes. The recommendation is to install or re-install a desired isolation tool, and ensuring its executable is found in the default PATH of your OS.
- Check the status. With the minikube status command, we display the status of the Minikube cluster. Open the PowerShell using the Run as Administrator option and execute the following command:
```
PS C:\WINDOWS\system32> minikube status

minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```
- Stop Minikube. We can stop Minikube using the minikube stop command. Open the PowerShell using the Run as Administrator option and execute the following command:
```
PS C:\WINDOWS\system32> minikube stop

✋  Stopping node "minikube"  ...
🛑  1 nodes stopped.
```

---
### Advanced Minikube Features
- Now that we have familiarized ourselves with the default minikube start command, let's dive deeper into Minikube to understand some of its more advanced features.
- The minikube start by default selects a driver isolation software, such as a hypervisor or a container runtime, if one (VitualBox) or multiple are installed on the host workstation. 
- In addition it downloads the latest Kubernetes version components. 
- With the selected driver software it provisions a single VM named minikube (with hardware profile of CPUs=2, Memory=6GB, Disk=20GB) or container to host the default single-node all-in-one Kubernetes cluster. - Once the node is provisioned, it bootstraps the Kubernetes control plane (with the default kubeadm tool), and it installs the latest version of the default container runtime, Docker, that will serve as a running environment for the containerized applications we will deploy to the Kubernetes cluster. 
- The minikube start command generates a default minikube cluster with the specifications described above and it will store these specs so that we can re-start the default cluster whenever desired. 
- The object that stores the specifications of our cluster is called a profile. 
- As Minikube matures, so are its features and capabilities. With the introduction of profiles, Minikube allows users to create custom reusable clusters that can all be managed from a single command line client.
- The minikube profile command allows us to view the status of all our clusters in a table formatted output. Assuming we have created only the default minikube cluster, we could list the properties that define the default profile with:
```
$ minikube profile list

|----------|------------|---------|----------------|------|---------|---------|-------|
| Profile  | VM Driver  | Runtime |       IP       | Port | Version | Status  | Nodes |
|----------|------------|---------|----------------|------|---------|---------|-------|
| minikube | virtualbox | docker  | 192.168.59.100 | 8443 | v1.23.3 | Running |     1 |
|----------|------------|---------|----------------|------|---------|---------|-------|
```
- This table presents the columns associated with the default properties such as 
    - the **profile name**: minikube
    - the **isolation driver**: VirtualBox
    - the **container runtime**: Docker
    - the **Kubernetes version**: v1.23.3
    - the **status of the cluster** - running or stopped. 
    - the **number of nodes**: 1 by default
    - the **private IP address of the minikube cluster's control plane VirtualBox VM**
    - the **secure port** that exposes the API Server to cluster control plane components, agents and clients: 8443. 
- What if we desire to create several reusable clusters instead, with other drivers (Docker or Podman) for node isolation, or different Kubernetes versions (v1.22.2 or v1.23.1), another runtime (cri-o or containerd), and possibly 2, 3, or more nodes (if permitted by the resources of our host system)? 
- What if we desire to further customize the cluster with a specific networking option or plugin? 
- The minikube **start** and **stop** commands allow us to create such custom profiles with the **--profile or -p** flags. Several of the isolation drivers support creation of node VMs or node containers of custom sizes as well, features that we will not explore in this Book as not all are very stable at the time of this writing.
- Below are a few examples of more complex start commands that allow custom clusters to be created with Minikube. The assume that the desired driver software (Docker and/or Podman) has been installed on the host workstation. There is no need to download the desired CNI (network plugin) or the container runtime, they will be set up and enabled by Minikube on our behalf:
```
$ minikube start --kubernetes-version=v1.22.2 --driver=podman --profile minipod

$ minikube start --nodes=2 --kubernetes-version=v1.23.1 --driver=docker --profile doubledocker

$ minikube start --driver=virtualbox --nodes=3 --disk-size=10g --cpus=2 --memory=4g --kubernetes-version=v1.22.4 --cni=calico --container-runtime=containerd -p multivbox

$ minikube start --driver=docker --cpus=6 --memory=8g --kubernetes-version="1.23.2" -p largedock

$ minikube start --driver=virtualbox -n 3 --container-runtime=cri-o --cni=calico -p minibox
```
- Once multiple cluster profiles are available (the default minikube and custom minibox), the profiles table will look like this:
```
$ minikube profile list

|----------|------------|---------|----------------|------|---------|---------|-------|
| Profile  | VM Driver  | Runtime |       IP       | Port | Version | Status  | Nodes |
|----------|------------|---------|----------------|------|---------|---------|-------|
| minibox  | virtualbox | crio    | 192.168.59.101 | 8443 | v1.23.3 | Running |     3 |
| minikube | virtualbox | docker  | 192.168.59.100 | 8443 | v1.23.3 | Running |     1 |
|----------|------------|---------|----------------|------|---------|---------|-------|
```
- Most minikube commands, such as start, stop, node, etc. are profile aware, meaning that the user is required to specify the target cluster of the command, through its profile name. The default minikube cluster, however, can be managed without specifying its profile name. Stopping and re-starting the two clusters listed above, the minibox cluster and the default minikube: 
```
$ minikube stop -p minibox

$ minikube start -p minibox

$ minikube stop

$ minikube start
```
- **Additional helpful minikube commands**:
- To display the version of the current Minikube installation:
```
$ minikube version

minikube version: v1.25.2
commit: 362d5fdc0a3dbee389b3d3f1034e8023e72bd3a7
```
- Completion is a helpful post installation configuration to enable the minikube command to respond to typical auto-completion mechanisms, such as completing a command in the terminal by pressing the TAB key. 
- To enable completion for the bash shell on Ubuntu:
```
$ sudo apt install bash-completion

$ source /etc/bash_completion

$ source <(minikube completion bash)
```
- If needed, also run the following command:
```
$ minikube completion bash
```
- A command that allows users to list the nodes of a cluster, add new control plane or worker nodes, delete existing cluster nodes, start or stop individual nodes of a cluster:
```
$ minikube node list

minikube 192.168.59.100

$ minikube node list -p minibox

minibox   192.168.59.101
minibox-m02   192.168.59.102
minibox-m03   192.168.59.103
```
- To display the cluster control plane node's IP address, or another node's IP with the --node or -n flags:
```
$ minikube ip

192.168.59.100

$ minikube -p minibox ip

192.168.59.101

$ minikube -p minibox ip -n minibox-m02

192.168.59.102
```
- When a cluster configuration is no longer of use, the cluster's profile can be deleted. It is also a profile aware command - it deletes the default minikube cluster if no profile is specified, or a custom cluster if its profile is specified:
```
$ minikube delete

🔥  Deleting "minikube" in virtualbox ...
💀  Removed all traces of the "minikube" cluster.

$ minikube delete -p minibox

🔥  Deleting "minibox" in virtualbox ...
🔥  Deleting "minibox-m02" in virtualbox ...
🔥  Deleting "minibox-m03" in virtualbox ...
💀  Removed all traces of the "minibox" cluster.
```
- For additional commands and usage options please visit the Minikube [command line reference](https://minikube.sigs.k8s.io/docs/commands/). 

---
### Exploring Minikube Profiles (Demo)
<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="path/to/poster_image.png">
    <source src="../images-docker-kubernetes/ch06-vid02-minikube-profiles-en" type="video/mp4">
    <!-- <source src="path/to/video.ogg" type="video/ogg"> -->
    <!-- <source src="path/to/video.webm" type="video/webm"> -->
  </video>
</figure>
<!-- blank line -->

---
### Learning Objectives (Review)
By the end of this chapter, you should be able to:
- Understand Minikube.
- Install Minikube on the native OS of your workstation.
- Verify the local installation.