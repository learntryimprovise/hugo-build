+++
title = "Kubernetes"
weight = 10004
chapter = false
+++

---
### Introduction
- In this chapter, we describe Kubernetes, its features, and the reasons why you should use it. We will explore the evolution of Kubernetes from Borg, Google's very own distributed workload manager.
- We will also learn about the Cloud Native Computing Foundation (CNCF), which currently hosts the Kubernetes project, along with other popular cloud-native projects, such as Prometheus, Fluentd, cri-o, containerd, Helm, Envoy, and Contour, just to name a few.

---
### What Is Kubernetes?
- According to the [Kubernetes](https://kubernetes.io/) website,
```
"Kubernetes is an open-source system for automating deployment, scaling, and management of containerized applications".
```
- **Kubernetes** comes from the Greek word **κυβερνήτης**, which means helmsman or ship pilot. With this analogy in mind, we can think of Kubernetes as the pilot on a ship of containers.

---
### From Borg to Kubernetes
- According to the abstract of [Google's Borg paper](https://research.google.com/pubs/pub43438.html), published in 2015,
```
"Google's Borg system is a cluster manager that runs hundreds of thousands of jobs, from many thousands of different applications, across a number of clusters each with up to tens of thousands of machines".
```
- For more than a decade, Borg has been Google's secret, running its worldwide containerized workloads in production. Services we use from Google, such as Gmail, Drive, Maps, Docs, etc., they are all serviced using Borg. 
- Among the initial authors of Kubernetes were Google employees who have used Borg and developed it in the past. They poured in their valuable knowledge and experience while designing Kubernetes. Several features/objects of Kubernetes that can be traced back to Borg, or to lessons learned from it, are:
    - API servers
    - Pods
    - IP-per-Pod
    - Services
    - Labels.
- We will explore all of them, and more, in this course.

---
### Kubernetes Features
Kubernetes offers a very rich set of features for container orchestration. Some of its fully supported features are:
- Automatic bin packing
- Kubernetes automatically schedules containers based on resource needs and constraints, to maximize utilization without sacrificing availability.
- Designed for extensibility
- A Kubernetes cluster can be extended with new custom features without modifying the upstream source code.
- Self-healing
- Kubernetes automatically replaces and reschedules containers from failed nodes. It terminates and then restarts containers that become unresponsive to health checks, based on existing rules/policy. It also prevents traffic from being routed to unresponsive containers.
- Horizontal scaling
- With Kubernetes applications are scaled manually or automatically based on CPU or custom metrics utilization.
- Service discovery and load balancing
- Containers receive IP addresses from Kubernetes, while it assigns a single Domain Name System (DNS) name to a set of containers to aid in load-balancing requests across the containers of the set.

Additional fully supported Kubernetes features are:
- Automated rollouts and rollbacks
- Kubernetes seamlessly rolls out and rolls back application updates and configuration changes, constantly monitoring the application's health to prevent any downtime.
- Secret and configuration management
- Kubernetes manages sensitive data and configuration details for an application separately from the container image, in order to avoid a re-build of the respective image. Secrets consist of sensitive/confidential information passed to the application without revealing the sensitive content to the stack configuration, like on GitHub.
- Storage orchestration
- Kubernetes automatically mounts software-defined storage (SDS) solutions to containers from local storage, external cloud providers, distributed storage, or network storage systems.
- Batch execution
- Kubernetes supports batch execution, long-running jobs, and replaces failed containers.
- IPv4/IPv6 dual-stack
- Kubernetes supports both IPv4 and IPv6 addresses.

There are many additional features currently in alpha or beta phase. They will add great value to any Kubernetes deployment once they become stable features. For example, support for role-based access control (RBAC) is stable only as of the Kubernetes 1.8 release.

---
### Why Use Kubernetes?
- Another one of Kubernetes' strengths is portability. It can be deployed in many environments such as local or remote Virtual Machines, bare metal, or in public/private/hybrid/multi-cloud setups.
-Kubernetes extensibility allows it to support and to be supported by many 3rd party open source tools which enhance Kubernetes' capabilities and provide a feature-rich experience to its users. It's architecture is modular and pluggable. Not only that it orchestrates modular, decoupled microservices type applications, but also its architecture follows decoupled microservices patterns. Kubernetes' functionality can be extended by writing custom resources, operators, custom APIs, scheduling rules or plugins.
- For a successful open source project, the community is as important as having great code. Kubernetes is supported by a thriving community across the world. It has more than 3,100 contributors, who, over time, have pushed over 106,000 commits. There are meet-up groups in different cities and countries which meet regularly to discuss Kubernetes and its ecosystem. The community is divided into Special Interest Groups (SIGs), groups which focus on special topics, such as scaling, bare metal, networking, storage, etc. We will learn more about them in our last chapter, Kubernetes Community. 

---
### Cloud Native Computing Foundation (CNCF)
- The [Cloud Native Computing Foundation (CNCF)](https://www.cncf.io/) is one of the largest sub-projects hosted by the [Linux Foundation](https://www.linuxfoundation.org/). CNCF aims to accelerate the adoption of containers, microservices, and cloud-native applications.
- CNCF hosts a multitude of projects, with more to be added in the future. CNCF provides resources to each of the projects, but, at the same time, each project continues to operate independently under its pre-existing governance structure and with its existing maintainers. Projects within CNCF are categorized based on their maturity levels: Sandbox, Incubating, and Graduated. At the time of this writing, over a dozen projects had reached Graduated status with many more Incubating and in the Sandbox.

Popular graduated projects:

    - Kubernetes container orchestrator
    - etcd distributed key-value store
    - CoreDNS DNS server
    - containerd container runtime
    - Envoy cloud native proxy
    - Fluentd for unified logging
    - Harbor registry
    - Helm package management for Kubernetes
    - Linkerd service mesh for Kubernetes
    - Open Policy Agent policy engine
    - Prometheus monitoring system and time series DB
    - Rook cloud native storage orchestrator for Kubernetes

Key incubating projects:

    - argo workflow engine for Kubernetes
    - Buildpacks.io builds application images
    - CRI-O container runtime
    - Contour ingress controller for Kubernetes
    - gRPC for remote procedure call (RPC)
    - CNI for Linux containers networking
    - flux continuous delivery for Kubernetes
    - Knative serverless containers in Kubernetes
    - KubeVirt Kubernetes based Virtual Machine manager
    - Notary for data security
    - And many more.

- There are many dynamic projects in the CNCF Sandbox geared towards metrics, monitoring, identity, scripting, serverless, nodeless, edge, expecting to achieve Incubating and possibly Graduated status. While many active projects are preparing for takeoff, others are being archived once they become less active and no longer in demand. The first projects to be archived are the rkt container runtime and the distributed OpenTracing.
- The projects under CNCF cover the entire lifecycle of a cloud-native application, from its execution using container runtimes, to its monitoring and logging. This is very important to meet the goals of CNCF.

---
### CNCF and Kubernetes
For Kubernetes, the Cloud Native Computing Foundation:

- Provides a neutral home for the Kubernetes trademark and enforces proper usage.
- Provides license scanning of core and vendor code.
- Offers legal guidance on patent and copyright issues.
- Creates and maintains open source learning curriculum, training, and certification for Kubernetes and cloud native associates (KCNA), administrators (CKA), application developers (CKAD), and security specialists (CKS).
- Manages a software conformance working group.
- Actively markets Kubernetes.
- Supports ad hoc activities.
- Sponsors conferences and meetup events.

---
### Learning Objectives (Review)
By the end of this chapter, you should be able to:
- Define Kubernetes.
- Explain the reasons for using Kubernetes.
- Discuss the features of Kubernetes.
- Discuss the evolution of Kubernetes from Borg.
- Explain the role of the Cloud Native Computing Foundation.