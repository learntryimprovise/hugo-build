+++
title = "Kubernetes Community"
weight = 10017
chapter = false
+++
---
### Introduction
- Just as with any other open source project, the community plays a vital role in the development of Kubernetes. 
- The community decides the road-map of the projects and works towards it. The community becomes engaged in different online and offline forums, like Meetups, Slack, Weekly meetings, etc. In this chapter, we will explore the Kubernetes community and see how you can become a part of it, too. 
---
### Kubernetes Community
- With over [85K GitHub stars](https://github.com/kubernetes/kubernetes/), Kubernetes is one of the most popular open source projects. The community members not only help with the source code, but they also help with sharing the knowledge. The community engages in both online and offline activities.
- Currently, there is a project called [K8s Port](https://k8sport.wufoo.com/forms/sign-up/), which recognizes and rewards community members for their contributions to Kubernetes. This contribution can be in the form of code, attending and speaking at meetups, answering questions on Stack Overflow, etc.
- Next, we will review some of the mediums used by the Kubernetes community.
---
### Weekly Meetings and Meetup Groups
- **Weekly Meetings** - A weekly community meeting happens using video conference tools. You can request a calendar invite from [here](https://groups.google.com/forum/#!forum/kubernetes-community-video-chat).
- **Meetup Groups** - There are many [meetup groups](https://www.meetup.com/topics/kubernetes/) around the world, where local community members meet at regular intervals to discuss Kubernetes and its ecosystem.
- **Kubernetes Community Days** - With [events](https://kubernetescommunitydays.org/) in Amsterdam, Bengaluru, Berlin, London, Montreal, Munich, Portland, Paris, Quebec, Tokyo, and Washington DC. 
- **Cloud Native Community Groups** - With [events](https://community.cncf.io/) in Canada, Mexico, and United States.
- There are some online meetup groups as well, where community members can meet virtually.
---
### Slack Channels and Mailing Lists
- **Slack Channels** - Community members are very active on the [Kubernetes Slack](https://kubernetes.slack.com/). There are different channels based on topics, and anyone can join and participate in the discussions. You can discuss with the Kubernetes team on the #kubernetes-users channel. 
- **Mailing Lists** - There are Kubernetes [users](https://groups.google.com/forum/#!forum/kubernetes-users) and [developers](https://groups.google.com/forum/#!forum/kubernetes-dev) mailing lists, which can be joined by anybody interested.
---
### SIGs and Stack Overflow
- **Special Interest Groups** - Focus on specific parts of the Kubernetes project, like scheduling, authorization, networking, documentation, etc. Each group may have a different workflow, based on its specific requirements. A list with all the current SIGs can be found [here](https://github.com/kubernetes/community/blob/master/sig-list.md). Depending on the need, a new SIG can be created.
- **Stack Overflow** - Besides Slack and mailing lists, community members can get support from [Stack Overflow](https://stackoverflow.com/questions/tagged/kubernetes), as well. Stack Overflow is an online environment where you can post questions that you cannot find an answer for. The Kubernetes team also monitors the posts tagged Kubernetes.
---
### CNCF Events
- CNCF organizes numerous international conferences on Kubernetes, as well as other CNCF projects. For more information about these events, please click [here](https://www.cncf.io/community/kubecon-cloudnativecon-events/).
- Three of the major conferences it organizes are:
> - KubeCon + CloudNativeCon Europe
> - KubeCon + CloudNativeCon North America
> - KubeCon + CloudNativeCon China.
- Other events are:
> - Envoycon
> - Open Source Summit Europe
> - Open Source Summit Japan. 
---
### What's Next on Your Kubernetes Journey?
Now that you have a better understanding of Kubernetes, you can continue your journey by:

> - Participating in activities and discussions organized by the Kubernetes community.
> - Attending events organized by the Cloud Native Computing Foundation and The Linux Foundation.
> - Advancing your learning and getting certified. The Linux Foundation and CNCF offer a range of different training and certification options (more on this on the next pages).
> - Contributing to the project (code, documentation, etc.).
> - And many other options.
---
### Kubernetes and Cloud Native Associate
If you are at the beginning of your Kubernetes journey, and don't yet feel ready to undertake a professional certification track, you can start with the Kubernetes and Cloud Native track, which consists of the Kubernetes and Cloud Native Essentials (LFS250) course and the Kubernetes and Cloud Native Associate (KCNA) exam:

- [Kubernetes and Cloud Native Essentials (LFS250)](https://training.linuxfoundation.org/training/kubernetes-and-cloud-native-essentials-lfs250/) - This course is designed for existing and aspiring developers, administrators, architects and managers who are new to the world of cloud native technologies and container orchestration. It gives you an overview of cloud native technologies, and then dives into container orchestration. You will review the high-level architecture of Kubernetes, understand the challenges of container orchestration and how to deliver and monitor your applications in distributed environments. The course will also discuss how container orchestration differs from legacy environments and much more.
- [Kubernetes and Cloud Native Associate (KCNA)](https://training.linuxfoundation.org/certification/kubernetes-cloud-native-associate/) - The KCNA exam is a pre-professional certification designed for candidates interested in advancing to the professional level through a demonstrated understanding of Kubernetes foundational knowledge and skills. This certification is ideal for students learning about or candidates interested in working with cloud native technologies. KCNA will demonstrate a candidate’s basic knowledge of Kubernetes and cloud-native technologies, including how to deploy an application using basic kubectl commands, the architecture of Kubernetes (containers, pods, nodes, clusters), understanding the cloud-native landscape and projects (storage, networking, GitOps, service mesh), and understanding the principles of cloud-native security.
---
### Certified Kubernetes Administrator
If you’re interested in Kubernetes administration, you can continue your journey with the Kubernetes Fundamentals (LFS258) course and the Certified Kubernetes Administrator (CKA) exam:

- [Kubernetes Fundamentals (LFS258)](https://training.linuxfoundation.org/training/kubernetes-fundamentals/) - This self-paced course will teach you how to use the container management platform used by companies like Google to manage their application infrastructure. You will learn how to install and configure a production-grade Kubernetes cluster, from network configuration to upgrades to making deployments available via services. The course also distills key principles, such as pods, deployments, replicasets, and services, and will give you enough information so that you can start using Kubernetes on your own. Please note that this course also has a 4-day instructor-led option, Kubernetes Administration (LFS458).
- [Certified Kubernetes Administrator (CKA)](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/) - This certification is for Kubernetes administrators, cloud administrators and other IT professionals who manage Kubernetes instances. CKA was created by The Linux Foundation and the Cloud Native Computing Foundation (CNCF) as a part of their ongoing effort to help develop the Kubernetes ecosystem. The exam is an online, proctored, performance-based test that requires solving multiple tasks from a command line running Kubernetes. A certified K8s administrator has demonstrated the ability to do basic installation as well as configuring and managing production-grade Kubernetes clusters. They will have an understanding of key concepts such as Kubernetes networking, storage, security, maintenance, logging and monitoring, application lifecycle, troubleshooting, API object primitives and the ability to establish basic use-cases for end users.
---
### Certified Kubernetes Application Developer

If you’re interested in Kubernetes application development, you can continue your journey with the Kubernetes for Developers (LFD259) course and the Certified Kubernetes Application Developer (CKAD) exam:

- [Kubernetes for Developers (LFD259)](https://training.linuxfoundation.org/training/kubernetes-for-developers/) - This self-paced course will teach you how to containerize, host, deploy, and configure an application in a multi-node cluster. Starting with a simple Python script, you will define application resources and use core primitives to build, monitor and troubleshoot scalable applications in Kubernetes. Working with network plugins, security and cloud storage, you will be exposed to many of the features needed to deploy an application in a production environment. Please note that this course also has a 3-day instructor-led option, Kubernetes for App Development (LFD459).
- [Certified Kubernetes Application Developer (CKAD)](https://training.linuxfoundation.org/certification/certified-kubernetes-application-developer-ckad/) - This certification is for Kubernetes engineers, cloud engineers and other IT professionals responsible for building, deploying, and configuring cloud native applications with Kubernetes. CKAD has been developed by The Linux Foundation and the Cloud Native Computing Foundation (CNCF), to help expand the Kubernetes ecosystem through standardized training and certification. This exam is an online, proctored, performance-based test that consists of a set of performance-based tasks (problems) to be solved in a command line. The Certified Kubernetes Application Developer (CKAD) can design, build and deploy cloud-native applications for Kubernetes. A CKAD can define application resources and use Kubernetes core primitives to create/migrate, configure, expose and observe scalable applications. The exam assumes working knowledge of container runtimes and microservices architecture. You must be comfortable working with (OCI-compliant) container images, applying Cloud Native application concepts and architectures, working with and validating Kubernetes resource definitions, and more.
---
### Certified Kubernetes Security Specialist
If you are interested in Kubernetes security and want to prove that you are an accomplished Kubernetes practitioner, you can continue your Kubernetes journey with the Kubernetes Security Essentials (LFS260) course and the Certified Kubernetes Security Specialist (CKS) exam:

- [Kubernetes Security Essentials (LFS260)](https://training.linuxfoundation.org/training/kubernetes-security-essentials-lfs260/) - This self-paced course exposes you to knowledge and skills needed to maintain security in dynamic, multi-project environments. This course addresses security concerns for cloud production environments and covers topics related to the security container supply chain, discussing topics from before a cluster has been configured through deployment, and ongoing, as well as agile use, including where to find ongoing security and vulnerability information. The course includes hands-on labs to build and secure a Kubernetes cluster, as well as monitor and log security events. Please note that this course also has a 4-day instructor-led option, Kubernetes Security Fundamentals (LFS460). 
- [Certified Kubernetes Security Specialist (CKS)](https://training.linuxfoundation.org/certification/certified-kubernetes-security-specialist/) - The Certified Kubernetes Security Specialist (CKS) program provides assurance that a CKS has the skills, knowledge, and competence on a broad range of best practices for securing container-based applications and Kubernetes platforms during build, deployment and runtime, and is qualified to perform these tasks in a professional setting. CKS is a performance-based certification exam that tests candidates' knowledge of Kubernetes and cloud security in a simulated, real world environment. You must take and pass the Certified Kubernetes Administrator (CKA) exam prior to attempting the CKS exam. 
---
### Learning Objectives (Review)
By the end of this chapter, you should be able to:
- Understand the importance of Kubernetes community.
- Learn about the different channels to interact with the Kubernetes community.
- List major CNCF events.
---

