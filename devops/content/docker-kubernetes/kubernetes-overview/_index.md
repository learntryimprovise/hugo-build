+++
title = "Kubernetes Overview"
weight = 10001
chapter = false
pre = "<b>1. </b>"
+++

**Reference** : https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS158x+1T2022/home

- Are you beginning to use Kubernetes for container orchestration? 
- Do you need guidelines on how to start transforming your journey with Kubernetes and cloud-native patterns?
- Would you like to simplify software container orchestration and find a way to grow your use of Kubernetes without adding infrastructure complexity? 
- Then this is the right place for you!
- Here will discuss some of Kubernetes' basic concepts and talk about the architecture of the system, the problems it solves, and the model that it uses to handle
containerized deployments and scaling.0
- This Book offers an introduction to Kubernetes and includes technical instructions on how to deploy a stand-alone and multi-tier application. You will learn about ConfigMaps and Secrets,
and how to use Ingress.
- Upon completion, developers will have a solid understanding of the origin, architecture and building blocks for Kubernetes, and will be able to begin testing the new cloud-native pattern to
begin the cloud-native journey.

# Prerequisites
Basic knowledge of Linux Command Line Interface (CLI) and container technology like Docker and rkt is required.

# Book Learning Objectives
By the end of this course, you should be able to:
- Understand the origin, architecture, primary components and building blocks of Kubernetes.
- Set up and access a Kubernetes cluster using Minikube.
- Run applications on the deployed Kubernetes environment, and access the deployed applications.
- Understand the usefulness of Kubernetes communities, and how you can participate.
---
# Book Outline
- [Chapter 1. From Monolith to Microservices](/docker-kubernetes/kubernetes-overview/chapter-1-from-monolith-to-microservices)
- [Chapter 2. Container Orchestration](/docker-kubernetes/kubernetes-overview/chapter-2-container-orchestration)
- [Chapter 3. Kubernetes](/docker-kubernetes/kubernetes-overview/chapter-3-kubernetes)
- [Chapter 4. Kubernetes Architecture](/docker-kubernetes/kubernetes-overview/chapter-4-kubernetes-architecture)
- [Chapter 5. Installing Kubernetes](/docker-kubernetes/kubernetes-overview/chapter-5-installing-kubernetes)
- [Chapter 6. Minikube Installing Local Kubernetes Clusters](/docker-kubernetes/kubernetes-overview/chapter-6-minikube-installing-local-kubernetes-clusters)
- [Chapter 7. Accessing Minikube](/docker-kubernetes/kubernetes-overview/chapter-7-accessing-minikube)
- [Chapter 8. Kubernetes Building Blocks](/docker-kubernetes/kubernetes-overview/chapter-8-kubernetes-building-blocks)
- [Chapter 9. Authentication Authorization Admission Control](/docker-kubernetes/kubernetes-overview/chapter-9-authentication-authorization-admission-control)
- [Chapter 10. Services](/docker-kubernetes/kubernetes-overview/chapter-10-services)
- [Chapter 11. Deploying A Standalone Application](/docker-kubernetes/kubernetes-overview/chapter-11-deploying-a-stand-alone-application)
- [Chapter 12. Kubernetes Volume Management](/docker-kubernetes/kubernetes-overview/chapter-12-kubernetes-volume-management)
- [Chapter 13. Configmaps And Secrets](/docker-kubernetes/kubernetes-overview/chapter-13-configmaps-and-secrets)
- [Chapter 14. Ingress](/docker-kubernetes/kubernetes-overview/chapter-14-ingress)
- [Chapter 15. Advanced Topics](/docker-kubernetes/kubernetes-overview/chapter-15-advanced-topics)
- [Chapter 16. Kubernetes Community](/docker-kubernetes/kubernetes-overview/chapter-16-kubernetes-community)

---