+++
title = "Measurement clarity"
draft = false
weight = 5
+++

---
The fourth Guiding Principle of TeamOps:: **Measurement clarity**
- [Measurement clarity](#measurement-clarity)
- [Transparent measurements](#transparent-measurements)
- [Measure results, not hours](#measure-results-not-hours)
- [Prioritize due dates over scope](#prioritize-due-dates-over-scope)
- [Iteration](#iteration)

---
## Measurement clarity
- Outdated workforce supervision tactics often trigger bias and presenteeism, so results must be uniquely recorded, managed, and supported for the accurate tracking and evaluation of productivity.
- Like Peter Drucker famously said, “If you can't measure it, you can't manage it.” But for many generations of business, the primary success metric was physical presence – an employee needed to be in a certain building in order to be at work.
- TeamOps supports the revolution that work is a verb, not a noun. Therefore, teams should rely on measuring productivity, value, and results without depending on physical supervision. Reprioritizing what is measured, how it’s measured, and when it’s measurement enables a higher frequency of success analysis, higher accountability to objectives, lower workforce discrimination, and a wider reach of company communication.

---
## Transparent measurements
- Conventional management philosophies glorify metrics, which is a nonspecific term that often lacks connection to objectives, mission, values, workflows, strategy, or a shared reality. 
- TeamOps prefers [Key Performance Indicators (KPIs)](https://about.gitlab.com/company/kpis/), which are smaller increments linked to [Objectives and Key Results (OKRs)](https://about.gitlab.com/company/okrs/) that, well, indicate performance and offer greater context on daily operations and the relevance of ongoing productivity to a function or the entire company.
- While smaller than OKRs, KPIs are not dependent on them. In fact, the two should be symbiotic in nature – informing and influencing each other for greater operational visibility, tracking accuracy, and team empowerment. If you're not creating OKRs to improve KPIs, then you're either missing KPIs or you have the wrong OKRs.
- Crucially, KPIs for each function are [transparently shared](https://about.gitlab.com/handbook/values/#findability) across the organization. This enables everyone to contribute by creating visibility between departments.
- Here's an example: [Chief Executive Officer OKR and KPIs](https://about.gitlab.com/company/okrs/fy23-q3/)
- In Q3-FY23, a CEO OKR is [Improve user and wider-community engagement](https://about.gitlab.com/company/okrs/fy23-q3/). This is the initiative to improve a series of KPIs, a subset of which are documented below:
    - Evolve the resident contributor strategy by conducting 5 customer conversations with current “resident contributors” in seat
    - Certify 1,000 team members and 10,000 wider-community members in TeamOps
    - Enhance Corporate Processes and Successful Corporate Development Integration & Prospecting
- These are documented in a tool (GitLab uses Ally) that is accessible to the entire organization. Critically, any team member can see any other functions OKRs and KPIs for the quarter, reinforcing the [value of transparency](https://about.gitlab.com/handbook/values/#transparency).

---
## Measure results, not hours
- The goal of every operational model is to optimize the efficiency of producing results, but conventional teams make a critical error when they conflate “efficiency” to mean “speed.” This means time becomes the highest priority of the team, and working hours become a primary success metric for the organization.
- In organizations powered by TeamOps, team members understand that the root of “productivity” is “to produce,” and therefore focus on [executing business results](https://about.gitlab.com/handbook/values/#measure-results-not-hours), rather than [executing on presenteeism](https://about.gitlab.com/handbook/values/#measure-results-not-hours). All success measurements should be based on outputs, not inputs.
- Note that outputs aren’t just tangible deliverables. Results include any value to the shared reality that a team member contributes: helping a teammate, satisfying a customer, shipping code, brainstorming a new idea, writing a revision, or researching a competitor. All quantifiable reports, messages, insights, or submissions are evidence of time well spent.
- Here's an example: [Measuring impact of GitLab's 10 year campaign](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5507)
- A cross-functional effort was required to produce the 10 Years of GitLab integrated marketing campaign and [associated website](https://about.gitlab.com/ten/). A GitLab issue was established to explicitly define [elements to be tracked](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5507) and measured in order to provide an impact report.
- By focusing on results over hours spent (or if a given team member was online at a certain time, or in a certain office), everyone involved in the project can focus energy on execution.

---
## Prioritize due dates over scope
- Although time is not a success measurement, TeamOps requires due dates.
- This is not a means to create unnecessary rigidity or measure duration of contributions, but to force mechanisms that enable teams to execute on decisions and require accountability.
- A TeamOps organization will always [set a due date](https://about.gitlab.com/handbook/values/#set-a-due-date), and if necessary, will cut scope to meet the due date rather than postpone the date. 
- This forces the time to think iteratively, by saving some of the scope for a future objective (or future execution), while limiting the loss of momentum.
- Here's an example: [Maintaining a monthly release cadence for 10+ years](https://about.gitlab.com/releases/)
- As of April 30, 2022, GitLab has shipped a monthly product release for [127 consecutive quarters](https://ir.gitlab.com/news-releases/news-release-details/gitlab-reports-first-quarter-fiscal-year-2023-financial-results). That's over 10 years! A decade of consistent execution is made possible by [cutting scope](https://about.gitlab.com/handbook/values/#set-a-due-date) instead of pushing ship dates.

---
## Iteration
- Executing on a decision should not be binary, nor a one-time event. 
- TeamOps reframes execution as an ongoing series of [iterations](https://about.gitlab.com/handbook/values/#iteration) – small, compounding results – each one worthy of celebration. 
- This encourages [smaller steps](https://about.gitlab.com/handbook/values/#move-fast-by-shipping-the-minimal-viable-change), which are more amenable to feedback, course correction, and easier to deliver. By breaking decisions down into manageable components, results are more feasible.
- In conventional organizations, there's often inherent pressure to present a complete and polished project, document, or plan. This expectation slows progress and expends valuable time that could be used to exchange multiple rounds of feedback on smaller changes.
- A key aspect of TeamOps is incorporating [iteration](https://about.gitlab.com/handbook/values/#iteration) into every process and decision with a [low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame). 
- This means doing the smallest viable and valuable thing, and getting it out quickly for feedback. 
- Despite the initial discomfort that comes from sharing the [minimal viable change (MVC)](https://about.gitlab.com/handbook/values/#low-level-of-shame), iteration enables faster execution, a shorter feedback loop, and the ability to course-correct sooner.
- This philosophy mirrors the GitLab product from a cycle-time standpoint. GitLab is built to reduce the time between making a decision and getting the result to market. Iteration enables cycle time reduction to be applied in day-to-day decision making for any project, result, team, or company in any industry.
- **Tip**: To empower even more of your team to make fast decisions through iteration, consider hosting [Iteration Office Hours](https://www.youtube.com/watch?v=2eA7-E950ps).
- Here's an example: [Iterating on promotional videos to launch TeamOps](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/319)
- In the development of TeamOps, our team at GitLab aspired to produce [two high-quality videos](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/319) to introduce the first, internal iteration of this certification. 
- When it became clear that TeamOps's brand identity would be changing in the coming weeks, we decided to produce only one video — an example of [Minimal Viable Change (MVC)](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/319) — and iterate as new information came in. 
- This showcases a maturity in embracing iteration. It celebrates the [boring solution](https://about.gitlab.com/handbook/values/#boring-solutions) (one video, the minimum required to inform GitLab team members), and enabled faster decisions throughout the launch phase.

---
Reference : [Gitlab TeamOps](https://levelup.gitlab.com/learn/course/teamops)