+++
title = "Ansible Concepts"
weight = 6100
chapter = false
pre = "<b>1. </b>"
+++

- Introduction to Ansible
    - Infrastructure Management
    - What is Capacity Planning?
    - Major Capacity Planning Tools
    - Understanding Capacity Planning
    - Calculating Percentile
    - What is Provisioning?
    - Major Provisioning Tools
    - What is Deployment?
    - Major Deployment Tools
- Need for Automated Deployments
- Deployment Matrix
    - App Service
    - Data Service
- What is Ansible?
- History of Ansible
- Key Components in Ansible
    - Ansible Control Machine
    - Ansible Node
    - Ansible Playbook
    - Ansible Role
    - Ansible Galaxy
    - Ansible Tower
- Overview of Ansible
    - Architecture of Ansible-managed
    - Deployments
    - Ansible Control Machine
    - Ansible Node
    - Ansible Playbook
    - Ansible Role
    - Ansible Inventory
- Ansible vs Chef
- FAQ on Ansible
- Prerequisites for Ansible
    - Understanding YAML
- Ansible Control Machine
    - What is the Purpose of Ansible Control Machine?
    - Install Ansible
    - Significance of Ansible Control Machine Programs
    - Understanding Ansible Control Machine Configurations
    - Configure Ansible Control Machine
    - Configure Highly Scalable Ansible Control Machine
    - Understanding Ansible Control Machine System Paths 
    - Deep Dive into Ansible Control Machine
    - How Ansible Control Machine Executes Playbook/Role?
    - Do's and Dont's with Ansible Control Machine