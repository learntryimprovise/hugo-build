+++
title = "Ansible Roles Vault"
weight = 6300
chapter = false
pre = "<b>3. </b>"
+++

- Ansible Role
    - What is the Purpose of Ansible Role?
    - Structure of Ansible Role
    - Writing Ansible Role
    - Manage Ansible Roles
    - Do's and Dont's with Ansible Role
- Ansible Vault
    - What is the Purpose of Ansible Vault?
    - Create Encrypted File using Ansible Vault
    - Create Encrypted String using Ansible Vault
    - Manage Encryption using Ansible Vault