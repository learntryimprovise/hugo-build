+++
title = "Ansible Inventory Playbooks"
weight = 6200
chapter = false
pre = "<b>2. </b>"
+++

- Ansible Inventory
    - What is the Purpose of Ansible Inventory?
    - Manage Ansible Inventories
- Ansible Playbook
    - What is the Purpose of Ansible Playbook?
    - Structure of Ansible Playbook
    - Writing Ansible Playbook
    - Manage Ansible Playbooks
    - Do's and Dont's in Ansible Playbook