+++
title = "Ansible Tutorials"
weight = 6998
chapter = false
pre = "<b>5. </b>"
+++
## PRE-REQUISITS

- 4 VMS RUNNING ON AS BASE MACHINE WITH MINIMUM CONFIGURATION AS BELOW,

> 1. 512 MB RAM (MINIMUM)
> 2. 15 GB OS DISK (THIN PROVISIONED)
> 3. OS – “CENTOS” OR “UBUNTU” WITH INTERNET BASED REPOSITORY USAGE  
> 4. VALID SSH CONNECTION TO OS REPOSITORIES FOR ALL 4 VMS
> 5. VMS SHOULD BE IN SAME NETWORK OR REACHABLE TO EACH OTHER


- VIRTUAL BOX ON USER'S LAPTOP IS ALSO PREFERABLE IF ITS MINIMUM 8GB MEMORY WITH ALL ABOVE POINTS WITH CENTOS/UBUNTU AS OS OPTION ONLY

---
## INTRODUCTION

##### 0. [ORIENTATION TO THE TRAINING ENVIRONMENT AND FLOW](#) 
> 1. CONTROL MACHINE
> 2. TARGET MACHINES (CLIENT MACHINES)
> 3. INTERNET BASED REPOSITORY ACCESS

---
## SECTION 1 - INTRODUCING ANSIBLE (DAY 01)

##### 1. [WHY ANSIBLE?](#)
> 1. INFRASTRUCTURE AUTOMATION NEEDS OF INDUSTRY
> 2. CONFIGURATION MANAGEMENT NEEDS OF INDUSTRY
> 3. STRENGTHS OF ANSIBLE - CONFIGURATION MANAGEMENT AND ORCHESTRATION
> 4. CONTINUOUS INTEGRATION CAPABILITIES

##### 2. [ANSIBLE CONCEPTS AND ARCHITECTURE](#)
> 1. USING SSH EFFECTIVELY (TRADITIONAL WAY)
> 2. AUTOMATING SSH LOGIN BASED ITERATIONS (TRADITIONAL WAY)
> 3. ANSIBLE ARCHITECTURE (SSH AGENT)

##### 3. [INDUSTRY EXAMPLES OF ANSIBLE USAGE](#)
> 1. USE CASE 01 – WEB SERVER DEPLOYMENT
> 2. USE CASE 02 – SERVER SECURITY PATCHING
> 3. CASE STUDIES AVAILABLE ON ANSIBLE WEBSITE
> 4. GETTING INTO AUTOMATION MENTALITY

##### 4. [INSTALLING ANSIBLE](#)
> 1. YUM BASED INSTALLATION
> 2. COMPILED INSTALLATION
> 3. WHAT ALL THINGS PROVIDED AS PART OF INSTALLATION?

##### SECTION 1 - LAB WITH EXAMPLES

> 1. [EXAMPLE 101: YUM BASED INSTALLATION](#)
> 2. [EXAMPLE 102: APT-GET BASED INSTALLATION](#)
> 3. [EXAMPLE 103: COMPILED INSTALLATION](#)

---
## SECTION 2 – GETTING STARTED WITH ANSIBLE (DAY 01)
##### 5. [ANSIBLE INVENTORY](#)
> 1. WHY INVENTORY?
> 2. DEFINING HOSTS IN INVENTORY
>  - SINGLE HOST
>  - MULTIPLE HOSTS
>  - GROUP OF HOSTS
>  - GROUP OF GROUPS
> 3. OVERRIDING INVENTORY LOCATION
>  - INVENTORY LOCATION PRECEDENCE
>  - USING INVENTORY FILE AT ANY LOCATION
> 4. DYNAMIC INVENTORY

##### 6. [ANSIBLE CONFIGURATION FILES](#)
> 1. WHY CONFIGURATION FILES?
> 2. FEW CONFIGURATION OPTIONS
>  - INVENTORY = /ETC/ANSIBLE/HOSTS
>  - LIBRARY = /USR/SHARE/MY_MODULES/
>  - MODULE_UTILS = /USR/SHARE/MY_MODULE_UTILS/
>  - REMOTE_TMP = ~/.ANSIBLE/TMP
>  - LOCAL_TMP = ~/.ANSIBLE/TMP
>  - FORKS = 5
>  - POLL_INTERVAL = 15
>  - SUDO_USER = ROOT
>  - ASK_SUDO_PASS = TRUE
>  - REMOTE_PORT = 22
>  - HOST_KEY_CHECKING = FALSE
> 3. OVERRIDING CONFIGURATION OPTIONS

##### 7. [ANSIBLE AD HOC COMMANDS](#)
> 1. WHY ANSIBLE AD HOC COMMANDS?
> 2. WHAT IS A TASK?
> 3. HOW SUDO WORKS ON LINUX?
> 4. UNDERSTANDING STRUCTURE OF ANSIBLE AD HOC COMMANDS
>  - COMMAND
>  - HOST PATTERN
>  - MODULE
>  - ARGUMENTS
>  - EXTRA OPTIONS
> 5. PING MODULE](#)
> 6. SHELL MODULE](#)
> 7. SCRIPT MODULE](#)
> 8. AD HOC COMMAND EXECUTION (FROM INVENTORY)
>  - SINGLE HOST
>  - MULTIPLE HOSTS
>  - GROUP OF HOSTS
>  - ALL HOSTS
> 9. AD HOC COMMAND EXECUTION (NOT IN INVENTORY)
>  - SINGLE HOST
>  - MULTIPLE HOSTS
>  - GROUP OF HOSTS
>  - ALL HOSTS

##### 8. [MANAGING DYNAMIC INVENTORIES](#)
> 1. WHY DYNAMIC INVENTORIES?
> 2. SHELL SCRIPT BASED EXAMPLE OF DYNAMIC INVENTORY

##### 9. [CONFIGURING CONNECTIONS IN AD HOC COMMANDS](#)
> 1. UNDERSTANDING SSH CONNECTION NEEDS (TRADITIONAL WAY)
> 2. SSH KEY LOCATION
> 3. REMOTE SSH USER
> 4. FORK OR NUMBER OF PARALLEL CONNECTIONS
> 5. POLL INTERVAL
> 6. REMOTE SUDO (PASSWORD BASED + PASSWORDLESS)

##### SECTION 2 - LAB WITH EXAMPLES
> 1. [EXAMPLE 201: WORKING ON SINGLE HOST USING AD HOC COMMAND (DEFAULT + CUSTOM INVENTORY LOCATION)](#)
> 2. [EXAMPLE 202: WORKING ON MULTIPLE HOSTS USING AD HOC COMMAND (DEFAULT + CUSTOM INVENTORY LOCATION)](#)
> 3. [EXAMPLE 203: WORKING ON HOST GROUP USING AD HOC COMMAND (DEFAULT + CUSTOM INVENTORY LOCATION)](#)
> 4. [EXAMPLE 204: WORKING ON HOST GROUPS USING AD HOC COMMAND (DEFAULT + CUSTOM INVENTORY LOCATION)](#)
> 5. [EXAMPLE 205: WORKING ON SINGLE HOST USING AD HOC COMMAND (DEFAULT + CUSTOM INVENTORY LOCATION)](#)
> 6. [EXAMPLE 206: WORKING WITH DYNAMIC INVENTORY (SINGLE HOST + MULTIPLE HOSTS + HOST GROUP + HOST GROUPS)](#)
> 7. [EXAMPLE 207: USING CUSTOM KEY LOCATION IN AD HOC COMMAND](#)
> 8. [EXAMPLE 208: USING FORK IN AD HOC COMMANDS](#)
> 9. [EXAMPLE 209: USING REMOTE USER IN AD HOC COMMANDS](#)
> 10. [EXAMPLE 210: USING SUDO IN AD HOC COMMANDS](#)

---
## SECTION 3 - IMPLEMENTING PLAYBOOKS (DAY 02)

##### 10. [GETTING STARTED WITH PLAYBOOKS](#)
> 1. WHAT IS YAML?
> 2. UNDERSTANDING PLAYBOOK STRUCTURE (YAML FORMAT)
> 3. SAMPLE AD HOC COMMANDS
> 4. WRITING EQUIVALENT PLAYBOOK
> 5. EXECUTING YOUR FIRST PLAYBOOK (SYNTAX VERIFICATION/DEBUG/DRY RUN)

##### 11. [ATTRIBUTES IN PLAYBOOK](#)
> 1. HOSTS
> 2. BECOME
> 3. BECOME_USER
> 4. REMOTE_USER
> 5. GATHER_FACTS

##### 12. [USING ANSIBLE DOCUMENTATION](#)
> 1. ANSIBLE-DOC
> 2. ANSIBLE
> 3. ANSIBLE-PLAYBOOK
> 4. ANSIBLE-GALAXY

##### 13. [CONFIGURING CONNECTIONS FOR ANSIBLE PLAYBOOK
> 1. UNDERSTANDING SSH CONNECTION NEEDS (TRADITIONAL WAY)
> 2. SSH KEY LOCATION
> 3. REMOTE SSH USER
> 4. FORK OR NUMBER OF PARALLEL CONNECTIONS
> 5. POLL INTERVAL
> 6. REMOTE SUDO (PASSWORD BASED + PASSWORDLESS)

##### SECTION 3 - LAB WITH EXAMPLES
> 1. [EXAMPLE 301: WORKING ON SINGLE HOST USING PLAYBOOK (DEFAULT + CUSTOM INVENTORY LOCAION)](#)
> 2. [EXAMPLE 302: WORKING ON MULTIPLE HOSTS USING PLAYBOOK (DEFAULT + CUSTOM INVENTORY LOCAION)](#)
> 3. [EXAMPLE 303: WORKING ON HOST GROUP USING PLAYBOOK (DEFAULT + CUSTOM INVENTORY LOCAION)](#)
> 4. [EXAMPLE 304: WORKING ON HOST GROUPS USING PLAYBOOK (DEFAULT + CUSTOM INVENTORY LOCAION)](#)
> 5. [EXAMPLE 305: WORKING ON SINGLE HOST USING PLAYBOOK (DEFAULT + CUSTOM INVENTORY LOCAION)](#)
> 6. [EXAMPLE 306: WORKING WITH DYNAMIC INVENTORY USING PLAYBOOK (SINLGE HOST + MULTIPLE HOSTS + HOST GROUP + HOST GROUPS)](#)
> 7. [EXAMPLE 307: USING CUSTOM KEY LOCATION IN PLAYBOOK](#)
> 8. [EXAMPLE 308: USING FORK IN PLAYBOOK](#)
> 9. [EXAMPLE 309: USING REMOTE USER IN PLAYBOOK](#)
> 10. [EXAMPLE 310: USING SUDO IN PLAYBOOK](#)

---
## SECTION 4 – VARIABLES IN PLAYBOOKS (DAY 03)

##### 14. [WHY VARIABLES](#)
> 1. NAMING CONVENTION
> 2. SCOPE OF VARIABLES
>  - GLOBAL SCOPE
>  - PLAY SCOPE
>  - HOST SCOPE

##### 15. [TYPE OF VARIABLES (DEPENDING ON PLACES WHERE THOSE ARE DEFINED)](#)
> 1. HOST VARIABLES
>  - INVENTORY FILE
>  - INVENTORY FOLDER
> 2. HOSTGROUP VARIABLES
>  - INVENTORY FILE
>  - INVENTORY FOLDER
> 3. REGISTERED VARIABLES
> 4. DYNAMIC VARIABLES
> 5. INCLUDED VARIABLES
> 6. CUSTOM VARIABLES
> 7. SYSTEM VARIABLES (FACTS)

##### 16. [WORKING WITH FACTS (SYSTEM INFORMATION)](#)
> 1. GATHERING SYSTEM INFORMATION (TRADITIONAL WAY)
> 2. WHEN TO USE FACTS
> 3. FACTS FILTERS
> 4. CUSTOM FACTS

##### SECTION 4 - LAB WITH EXAMPLES
> 1. [EXAMPLE 401: USING VARIABLES DEFINED IN PLAYBOOK (TASKS DEFINED)](#) 
> 2. [EXAMPLE 402: USING VARIABLES DEFINED IN PLAYBOOK (VARS DEFINED)](#)
> 3. [EXAMPLE 403: USING SYSTEM FACTS IN PLAYBOOK](#)
> 4. [EXAMPLE 404: USING CUSTOM FACTS IN PLAYBOOK (JSON FORMAT)](#)
> 5. [EXAMPLE 405: USING CUSTOM FACTS IN PLAYBOOK (INI FORMAT)](#)
> 6. [EXAMPLE 406: USING VARIABLES INCLUDED FROM A FILE IN A PLAYBOOK (TASKS DEFINED)](#)
> 7. [EXAMPLE 407: USING VARIABLES INCLUDED FROM A FILE IN A PLAYBOOK (VAR_FILE DEFINED)](#)
> 8. [EXAMPLE 408: USING VARIABLES DEFINED IN INVENTORY FILE (DEFAULT + CUSTOM LOCATION INVENTORY)](#)
> 9. [EXAMPLE 409: USING VARIABLES DEFINED IN VARS FOLDER](#)
> 10. [EXAMPLE 410: USING VARIABLES DEFINED IN HOST_VARS AND GROUP_VARS FOLDERS](#)
> 11. [EXAMPLE 411: USING REGISTERED VARIABLES IN PLAYBOOK](#)

---
## SECTION 5 – ANSIBLE CONTROL MECHANISMS (DAY 04)

##### 17. [TASKS IN LOOPS](#)
 1. NO LOOP
 2. SIMPLE LOOP
 3. ITEMISED LOOP
 4. NESTED LOOP

##### 18. [TASKS WITH CONDITION/S](#)
 1. SIMPLE WHEN
 2. WHEN WITH VARIABLES
 3. MULTIPLE CONDITIONS IN WHEN

##### 19. [HANDLERS (TRIGGERS)](#)
 1. USING SINGLE HANDLER
 2. USING MULTIPLE HANDLERS

##### 20. [TAGS (LABEL)](#)
 1. EXECUTING TAGGED TASKS
 2. EXECUTING UN-TAGGED TASKS

##### 21. [WORKING WITH ERRORS](#)
 1. FAILED_WHEN
 2. IGNORE_ERRORS
 3. FORCED_HANDLERS
 4. BLOCK

##### SECTION 5 - LAB WITH EXAMPLES
1. [EXAMPLE 501: PLAYBOOK WITHOUT LOOP](#)
2. [EXAMPLE 502: USING SIMPLE LOOP IN PLAYBOOK](#)
3. [EXAMPLE 503: USING ITEMISED LOOP IN PLAYBOOK](#)
4. [EXAMPLE 504: USING NESTED LOOP IN PLAYBOOK](#)
5. [EXAMPLE 505: USING WHEN CONDITION IN PLAYBOOK](#)
6. [EXAMPLE 506: USING WHEN CONDITION ON REGISTERED VARIABLES IN PLAYBOOK](#)
7. [EXAMPLE 507: USING WHEN CONDITION ON FACT VARIABLES IN PLAYBOOK](#)
8. [EXAMPLE 508: USING MULTIPLE CONDITIONS ON A SINGLE TASK IN A PLAYBOOK](#)
9. [EXAMPLE 509: USING SIMPLE LOOP WITH WHEN CONDITION](#)
10. [EXAMPLE 510: INVOKING SINGLE HANDLER IN PLAYBOOK](#)
11. [EXAMPLE 511: INVOKING MULTIPLE HANDLERS IN PLAYBOOK](#)
12. [EXAMPLE 512: USINGS TAGGED PLAYBOOK FOR TWO DIFFERENT FLOW OF TASKS BASED ON END GOAL/REQUIREMENT](#)
13. [EXAMPLE 513: PLAYBOOK FOR IGNORING ERRORS ON FEW TASKS](#)
14. [EXAMPLE 514: FAILING A PLAYBOOK ON A SPECIFIC CONDITION](#)
15. [EXAMPLE 515: USING FORCED HANDLERS IN PLAYBOOK](#)
16. [EXAMPLE 516: USING BLOCK IN PLAYBOOK FOR ERROR HANDLING](#)

---
## SECTION 6 - JINJA (DAY 04)

##### 22. [WHAT IS TEMPLATES?](#)

##### 23. [JINJA TEMPLATES](#)
 1. DELIMITERS
 2. LOOPS
 3. CONDITIONS

##### 24. [KNOWN ISSUES](#)

##### SECTION 6 - LAB WITH EXAMPLES
1. [EXAMPLE 601: USING JINJA TEMPLATING TO PUSH A FILE WITH DYNAMIC CONTENT IN A PLAYBOOK](#)
2. [EXAMPLE 602: USING JINJA LOOP IN A PLAYBOOK](#)
3. [EXAMPLE 602: USING JINJA CONDITION IN A PLAYBOOK](#)

---
## SECTION 7 – ANSIBLE ROLES (DAY 05)

##### 25. [WHY ROLES NEEDED?](#)

##### 26. [STRUCTURE OF A ROLE](#)

##### 27. [ROLE WITH DEPENDENCIES](#)

##### 28. [TYPE OF ROLES ANSIBLE GALAXY](#)
 1. CUSTOM ROLES
 2. ROLES FROM ANSIBLE GALAXY

##### SECTION 7 - LAB WITH EXAMPLES
1. [EXAMPLE 701: WRITING A PLAYBOOK FOR INSTALLATION OF APACHE WITH BELOW CRITERIA](#)
2. [EXAMPLE 702: WRITING A PLAYBOOK FOR INSTALLATION OF MARIADB WITH BELOW CRITERIA](#)
3. [EXAMPLE 703: WRITING A PLAYBOOK TO GET APACHE AND MYSQL INSTALLED USING PREVIOUSLY DESCRIBED CRITERIA (WITHOUT ROLE CONCEPT)](#)
4. [EXAMPLE 704: WRITING A PLAYBOOK TO GET APACHE AND MYSQL INSTALLED USING PREVIOUSLY DESCRIBED CRITERIA (WITHOUT ROLE CONCEPT)](#)
5. [EXAMPLE 705: INSTALLING A ROLE FROM GALAXY AND USING IT STANDALONE](#)
6. [EXAMPLE 706: INSTALLING A ROLE FROM GALAXY AND USING IT IN ANOTHER PLAYBOOK LOGIC](#)
7. [EXAMPLE 707: MAKING CUSTOM MADE APACHE AND MYSQL ROLES AVAILABLE AS DEFAULT ROLES](#)

---
## SECTION 8 - ANSIBLE VAULT (DAY 05)

##### 29. [WHY VAULT NEEDED?](#)

##### 30. [COMMANDS USED](#)

##### 31. [USING VAULT WITH AD HOC COMMANDS](#)

##### 32. [USING VAULT WITH PLAYBOOKS](#)

##### SECTION 8 - LAB WITH EXAMPLES
1. [EXAMPLE 801: WRITE PLAYBOOK FOR INSTALLATION OF MARIADB AND SET ROOT PASSWORD FOR THE SAME USING VAULT PROTECTED PASSWORD FILE](#)
2. [EXAMPLE 802: WRITE PLAYBOOK FOR INSTALLATION OF MARIADB AND SET ROOT PASSWORD FOR THE SAME USING VAULT PROTECTED VARIABLE FILE](#)

---
## SECTION 9 – BEST PRACTICES (DAY 05)

##### 33. [APPROACH TO WRITE PLAYBOOK](#)

##### 34. [DELEGATION](#)

##### 35. [PARALLELISM](#)

##### 36. [USING POWER PREVILEDGE](#)

##### 37. [INVENTORY FILE PLACEMENT](#)

##### 38. [PASSWORD AND KEYS](#)

##### 39. [PLAYBOOK BEST PRACTICES](#)

##### 40. [TROUBLESHOOTING](#)

##### SECTION 9 - LAB WITH EXAMPLES
1. [EXAMPLE 901: USING DELEGATION IN PLAYBOOK](#)
2. [EXAMPLE 902: USING PARALLELISM IN PLAYBOOK FOR SEQUENCING](#)
3. [EXAMPLE 903: SEGRAGATING NORMAL AND PREVILEDGED TASKS IN A PLAYBOOK](#)
4. [EXAMPLE 904: EXECUTING SPECICIF TASK ON ANOTHER HOST DEFINED IN INVENTORY FROM A PLAYBOOK](#)
5. [EXAMPLE 905: EXECUTING SPECICIF TASK ON A HOST DEFINED OUTSIDE INVENTORY FROM A PLAYBOOK](#)
6. [EXAMPLE 906: WRITE A PLAYBOOK TO REBOOT A SERVER ONE BY ONE BY WAITING FOR REBOOTED ONE TO GET ONLINE FOR SSH CONNECTION (ASYNC)](#)

---
## ANNEXURE I – FREQUENTLY USED MODULES

1. [PING](#)
2. [SHELL](#)
3. [FILE](#)
4. [COPY](#)
5. [YUM](#)
6. [SERVICE](#)
7. [USER](#)
8. [PARTED](#)
9. [FILESYSTEM](#)
10. [MOUNT](#)
11. [STAT](#)
12. [TEMPLATE](#)
13. [WAIT_FOR](#)
14. [FIREWALLD](#)
15. [UNARCHIVE](#)
16. [SETUP](#)
17. [FAIL](#)
18. [HAPROXY](#)
19. [AWS_S3](#)
20. [EC2](#)
21. [EC2_AMI](#)
22. [AZURE](#)
23. [VMWARE](#)

---
## ANNEXURE 2 - REFERENCES

---