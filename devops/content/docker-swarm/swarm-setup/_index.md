+++
title = "Swarm Setup"
weight = 9100
chapter = false
pre = "<b>2. </b>"
+++

- Setup Swarm
    - Boot Swarm Nodes
    - Install & Configure Docker
- Configure Swarm
    - Initialize Swarm Cluster
    - Join Nodes to Swarm Cluster
    - Prepare Nodes in Swarm Cluster
    - Manage Tokens in Swarm Cluster
    - Deep Dive into Swarm Cluster
- Swarm Node
    - Purpose of Swarm Node?
    - Provision Highly Scalable Swarm Nodes
    - Scaling Manager Swarm Nodes
    - Scaling Worker Swarm Nodes Quorum-based Stability of Manager Swarm Nodes
    - Manage Swarm Nodes
- Swarm Stack
    - Purpose of Swarm Stack?
    - Understanding Swarm Stack
    - Create Swarm Stack
    - Manage Swarm Stacks
- Swarm Service
    - Purpose of Swarm Service?
    - Ways to Expose Swarm Service
    - Manage Swarm Services