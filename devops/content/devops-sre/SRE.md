+++
title = "Site Reliability Engineering (SRE)"
weight = 8
chapter = false
+++

---
### Introduction to Site Reliability Engineering (SRE)
---
### What is SRE?
---
### SRE versus DevOps
---
### Measuring Reliability with SLIs
---
### Embracing Risks with SLOs and Error Budgets
---
### The 7 Principles of SRE
---
### 7 Tenets - SRE Best Practices
---
### The Path to Site Reliability
---
### Building SRE Practices in Your Organization

---
Reference : https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS162x+2T2022/home