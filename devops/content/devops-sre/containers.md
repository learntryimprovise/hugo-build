+++
title = "Containers"
weight = 3
chapter = false
+++

---
### Introduction to Containers
---
### Containers - Why Should You Bother?
---
### A Brief History of Containers
---
### Docker - What's Under the Hood?
---
### Kubernetes - Why Do We Need It?
---
### Demo 1: How Fast and Lightweight Are Containers vs. VMs?
---
### Demo 2: Launching Applications with Docker
---
### Demo 3: Packaging Applications with Docker Images
---
### Demo 4: Deploying Apps with Kubernetes
---
### Demo 5: Kubernetes Key Features: Scalability, Load Balancing, Autoscaling, High Availability
---
### Is Docker Dead?
---
### Emerging Container Technologies

---
Reference : https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS162x+2T2022/home