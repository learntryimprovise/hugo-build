+++
title = "Observability"
weight = 7
chapter = false
+++

---
### Introduction to Observability
---
### Observability - Why Should You Bother?
---
### Metrics, Logs and Traces: What To Observe and Why
---
### SLAs, SLOs, SLIs and Observability
---
### Demo: Prometheus and Grafana for Plotting Metrics
---
### Demo: Elasticsearch and Kibana
---
### Demo: Distributed Tracing with Jaeger

---
Reference : https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS162x+2T2022/home