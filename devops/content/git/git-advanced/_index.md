+++
title = "Git Advanced"
weight = 4200
chapter = false
pre = "<b>2. </b>"
+++

- Git Service Provider
- What is the Purpose of Git Service Provider?
- Major Git Service Providers
    - GitHub
    - Bitbucket
    - GitLab
- How to Choose a Git Service Provider?
- Manage SSH Keys with Git Service Provider
- Create Git Repository
- Manage Repositories in Git Service Provider
- Push Changes from Local Git Repository to Remote Git Repository
- Integrate Git Repository with Jenkins CI Server
- Do's and Dont's with Git Service Provider
- Git Workflow
    - What is Git Workflow?
    - What is the Purpose of Git Workflow?
    - Architecture of Git Workflow
    - Principles of Git Workflow
- Execute Git Workflow
    - Intialize Git Workflow
    - Clone Git Repository
    - Cherry Pick Git Commits from One Branch and Apply to Other Branch
    - Merge Git Branches
    - Create Release using Git Tag
    - Handle Bug After Production Deployment
- Do's and Dont's with Git Workflow
- Case Study
    - How We Implemented Git Workflow for SloopEngine Product Development
- Merge Conflict in Git
    - Why Merge Conflict Occurs in Git Repository?
    - Simulate Merge Conflict in Git Repository
    - Fix Merge Conflict in Git Repository
- Git Stash
    - What is the Purpose of Git Stash?
    - Stash Changes Before Switching to Another Git Branch
- Git Reset
    -  What is the Purpose of Git Reset?
    - Undo Commits on Branch using Git Reset
- Git Rebase
    - What is the Purpose of Git Rebase?
    - Rebasing Scenario in Git Repository
    - Rebasing a Branch using Git Rebase