+++
title = "DevOps & DevSecOps"
weight = 10
chapter = false
+++

---

- [Why Cybersecurity Matters](#why-cybersecurity-matters)
- [Cybersecurity 101 - Threats](#cybersecurity-101---threats)
- [Cybersecurity 101 - Secure Infrastructure](#cybersecurity-101---secure-infrastructure)
- [Cybersecurity 101 - Additional Concepts](#cybersecurity-101---additional-concepts)
- [Cybersecurity vs Compliance](#cybersecurity-vs-compliance)
- [The 'Sec' in DevSecOps](#the-sec-in-devsecops)
---
### Why Cybersecurity Matters
- **Malware** - A term for any type of code written or intended to produce some type of negative or adverse effect.
- **Cyber Ransom** - This is where an adversary or individual or organization, because more and more of these have become business strategies, and in a bad way, have produced an effect to lock up people's business systems and hold their data ransom for others. The reason why this is so widespread is that people have started developing business plans and business models around this, to where they're actually licensing the software that produces the attack.
- **Stuxnet & Cyber Terrorism** - Which was a cyberattack on the Iranian nuclear centrifuge facilities, which was looking at enriching uranium, and an unknown actor produced a cyber effect that not only took down the IT system, but produced an actual physical impact to the equipment that was resident there. Stuxnet demonstrated is the ability through virtualized space to cause real world harm to equipment, and in theory also then by extension people as well.
- **Solution** - Moving security towards left. 
---
### Cybersecurity 101 - Threats

![Threats](/images/devops-devsecops/09-threats.png "Threats Icon")

---
### Cybersecurity 101 - Secure Infrastructure
- Secure Devices (Servers/Laptops/Mobile Devices)
- Network Security
- Continuous Logging
- Continuous Monitoring

---
### Cybersecurity 101 - Additional Concepts
- Runtime Security
- Service Mesh
- Zero Trust
- Identity and Access Management (IDAM)
- Preferring **Declarative and Immutable** over **Imperative and Mutable**
- Encryption
- Air Gap Systems

---
### Cybersecurity vs Compliance
- [NIST 800-53](https://csrc.nist.gov/publications/detail/sp/800-53/rev-5/final)
---
### The 'Sec' in DevSecOps
- Fully integrated Security Team
- Security Automation
- Shift Security Towards Left
- Red Team - Team who don't know the system and try to finds vulnerabilities, may be a third party.
- Compliance Automation
- Defense in depth

---
Reference: https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS180x+2T2022/home
