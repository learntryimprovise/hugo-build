+++
title = "Background & History"
weight = 2
chapter = false
+++

---
- [What Is DevOps/DevSecOps?](#what-is-devopsdevsecops)
- [Historical Perspective](#historical-perspective)
- [The Agile Manifesto and Innovator's Dilemma](#the-agile-manifesto-and-innovators-dilemma)
- [Customer Discovery and The Lean Startup](#customer-discovery-and-the-lean-startup)
---
### What Is DevOps/DevSecOps?
- DevOps is one of those really overloaded terms and a lot of people equate it to tooling and/or developers and operations speaking to each other occasionally.
And it's just really not. 
- It started off with, how do we get common incentives for developers and operations? But it's really around improving the flow of value to the entire organization.
- And in 2015, Donovan Brown from Microsoft came up with this really great definition that I've used ever since. It's "**DevOps is the union of people, process, and products to enable the continuous delivery of value to the end user.**". And that really covers everything you need to know and think about if you're trying to say, "Hey, what do we need to do to make things better?"
- The original term was really looking at the unification of software development with IT operations. And We definitely want to stress IT operations.
- For any of those organizations that might consider ops or operational activities to be something other than IT, they sometimes confuse **DevOps** with say operators and putting developers next to operators. That's not DevOps.
- What is the difference between DevOps and DevSecOps? To be quite honest, there shouldn't be a difference and in fact, they imply the exact same thing.
- Security was always, should have been and could have been and is a part of DevOps.
- The reality is that there were people who were some subset, some percentage of people who were implementing DevOps concepts that weren't actually putting a security hat on
and hadn't fully integrated the security teams. And so the community to at least some degree, has inserted this concept of DevSecOps to where it's not only the unification of software development and IT operations, but also security. But of course, you could throw a bunch of different terms in there.
- A lot of times, We use DevSecOps, because it's very much a reminder that you have to put security first. And it's not that DevOps doesn't put security first. It's that by putting security first and actually into the word itself, it's a great reminder for people of the importance of cybersecurity.
---
### Historical Perspective

1. Galileo is known as the father of the scientific method, which is only used in science and not applicable to other fields.
    - False. We're actually always validating what we think to be true and assuming that it's not true and we run experiments to verify no matter what we're doing. And especially true if you're doing any sort of product development.
2. Frederick Winslow Taylor is known as the father of Scientific Management, which is only used in science and not applicable to other fields.
    - False. Initial use case in his case was optimization of specific tasks, which he referred to as scientific management. But it's certainly a lot of the... even though there are some negative aspects to Taylorism, the whole... people as cattle approach, there was at the time some very fantastic validating growth in thought around how do you actually take a process that used to be considered almost like art that you had to just learn by doing a full trade, and how do you actually capture the right metrics so you can line up the right steps so you can produce an end result.
3. Henry Ford is known as the father of the assembly line which is only used in hardware manufacturing and is not applicable to other fields.
    - False. It's anytime we have repeatable steps that we need to execute. We can use in a similar line approach. But again, software doesn't... everything before... anything up until we're finished coding is not a repeatable step.

![What History Gave to DevSecOps](/images/devops-devsecops/01-what-history-gave-to-devsecops.png "What History Gave to DevSecOps Icon")

4. One of Deming's 14 Total Quality Management principles is to remove annual ratings or merit systems.
    - True. In some companies there's pushing for this. It might be impossible to do for your organization. The key thing to take away is that consistent and timely feedback. When somebody's having an issue someone's not doing things according to the way that they should be doing, you shouldn't be waiting for an annual review. You should be addressing it on the spot. And in any organization where you have something that's bad, you can find ways to work around it to turn it into something that's less bad.
5. Toyota and Ohno are known for developing Lean Manufacturing, 7 Wastes and Just-in-Time delivery build, et cetera.
    - Yes.The principles, although initially designed for manufacturing are equally applicable to software baselines.

---
### The Agile Manifesto and Innovator's Dilemma
- 12 Agile Principles are base as shown in below diagram.

![12 Agile Principles](/images/devops-devsecops/01-12-agile-principles.png "12 Agile Principles Icon")

- **What did the Agile Manifesto teach us in regards to DevSecOps?**

The thing the Agile Manifesto brought was, just embracing reality, that product development and software is uncertain. That we need rapid feedback loops to make sure that we're on the right path so that we don't deliver too much waste. We will deliver some waste. This is just a fact, but the larger the delivery we make, the more risk there is that there's more waste, and that could be just unneeded features or defects or whatever.

- **What did the Innovator's Dilemma teach us about DevOps?**

It's that iterative improvements, although incredibly, incredibly valuable, there still is this concept of disruptive new technologies and processes. Experimentation is still true. And all the same principles are still true. But if all you're focused on is driving to exactly what the customer is asking for within what, on paper, looks to project, to be the largest things from an ROI perspective, then you will fail because nobody can predict the future.
And you need to also, in addition to your iterative improvements, be looking at experimenting with disruptive technologies because there will be breakthroughs across the community.

- The Agile Manifesto is a living breathing document that is constantly evolving with the latest and greatest information.

---
### Customer Discovery and The Lean Startup

![Customer Discovery](/images/devops-devsecops/01-customer-discovery.png "Customer Discovery Icon")

- Lean Startup is taking really the principles found in Customer Discovery, mixed with the Agile Manifesto.

![DevOps 3 Way](/images/devops-devsecops/01-devops-3-way.png "Devops 3 Way Icon")

---
Reference: https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS180x+2T2022/home