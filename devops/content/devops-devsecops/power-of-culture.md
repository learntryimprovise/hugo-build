+++
title = "Power of Culture"
weight = 7
chapter = false
+++

---
**If your culture is broken, your ability to deliver is broken**

- [Where Does Culture Fit?](#where-does-culture-fit)
- [Culture and HR](#culture-and-hr)
- [How to Drive Culture?](#how-to-drive-culture)
- [GitOps for Culture](#gitops-for-culture)
- [Insubordination](#insubordination)
- [What to Do if Your Organization Is Culturally Broken](#what-to-do-if-your-organization-is-culturally-broken)
---
### Where Does Culture Fit?
- Any organization is a Pyramid of **People, Culture, Process, Tech**

![Pyramid of Organization](/images/devops-devsecops/06-pyramid-of-organization.png "Pyramid of Organization")

- **People** and **Culture** matter more than tools you are using to deliver.
---
### Culture and HR
- **Why HR matters for any organization?**
    - How you recruit People matters
    - How you hire People matters
    - How you treat People matters
    - How you inspire People matters
    - How you lead People matters
    - How you follow People matters
---
### How to Drive Culture?
- **Who Drives Culture?** - Entire organization.
- **Is Culture intentional or Accident?** - Obviously it should be intentional.

---
### GitOps for Culture
- **What GitOps Means?** - GitOps is really looking at declaratively updating an Ops or production system looking at either your, generally, infrastructure as code and configuration as code, which is typically managed within some Git technology, GitLab, GitHub, and continuously pushing out updates by changing the actual Git repository itself which then gets pulled into the production or operational environment and reflects it as a declarative baseline update in production.
- **How to Declare Culture?** - this can be organization level policies. Handbook can be a good place to get that documented and made available for all people
- **How to Measure Culture?** - regular surveys, discussions can be mechanisms to measure culture. More from HR point of view to check happiness index at various level of organization.
- **How to Change Culture?** - This can be hard for a well established team or organization, but achievable, Provided Intent for the change of Culture is getting value added to multiple levels  of organization.
---

### Insubordination
- Understand what insubordination is first. Insubordination by definition: "Defiance of authority; refusal to obey orders"
- Insubordination if used in a positive way can results in right results.
---

### What to Do if Your Organization Is Culturally Broken
- Leadership and Management needs to become the champion of the culture you want to have
- Find people within your own organization who exemplify the culture you want and then do everything you can to support them.
- Go out, recruit external people with similar culture and skill-set and do everything you can to hire them quickly
- Start Small, always for any cultural corrections/changes
- To Scale, you must Descale your organization
- Be Patient and Stay Committed
- Forgive and Forget Easily (Don't take it Personally)
- The best way to fix a large organization's culture is to get it right within a few small teams and then grow the number of those small teams over time.
- A blocker or saboteur one day may be one of your biggest supporters the next day. Not always but can be a game changer for organization with broken culture.

---
Reference: https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS180x+2T2022/home