+++
title = "DevOps & DevSecOps"
weight = 5
chapter = false
+++

---
- [Platform: Mission & Goals](#platform-mission--goals)
- [Platform as a Trusted Product](#platform-as-a-trusted-product)
- [Productizing the Platform](#productizing-the-platform)
- [Scaling Our Platform](#scaling-our-platform)
---
### Platform: Mission & Goals
- **Mission of a platform** is "union of people, process, and product to enable continuous value delivery to the end user".
- ***Platform Product goals***
    - A platform to be anything we're doing to try to remove friction from the process. And so that really kind of goes to our platform goals, you know, ***reduce delivery friction***.
    - ***Reduce the cost of change*** - People focus on that enough, if we measure the cost of change over time. And try to drive that down, we can become more efficient.
    - ***Make the right things easy*** - Now this, there's a corollary to this, which is to make the things that we don't want to happen more difficult, or impossible, depending what it is, but we definitely want to make the right things easy.
    - And as a platform team, I believe that our job is to ***help other teams excel*** We elevate other teams. That is our job.
- Changing organization topology to have platform as a product as shown in below diagram. 

![Platform a Product Topology change](/images/devops-devsecops/04-change-org-topology-for-platform.png "Platform a Product Topology change Icon")

---
### Platform as a Trusted Product
- **Mission** - Partner with other product teams to help them quickly, safely and securely deliver value to their end users 
- **Vision** - Irresistible developer experience
- Deploying the best possible delivery tools will not solve the problem and tools without guidance can cause more problems than they solve. 

---
### Productizing the Platform
- Implement Organizational Goals
    - Rapid Onboarding
    - Increased Reuse
    - Automate Process Overhead
    - Security and Compliance 
    - Continuous Delivery
- Focus on Customer needs
    - Easier to use
    - Good Support and Training 
    - User outreach and meetups
    - Community Building
- Make the Right things easy
    - Templates for preferred patterns
    - Seed Applications 
    - Common developer experience
    - Useful documentation    

---
### Scaling Our Platform
- Scalable Operations 
    - Documentation and Training 
    - Customer Support
    - Self Service Tools
    - Enable Teams to help themselves 
- Eat our Own Dog Food
    - Use our platform to deliver our platform
    - Use our documentation to answer questions 
    - Build Examples with our products    
    - Help customers use our products
- a Platform doesn't matter if teams 
    - Dont know about it
    - Dont want to use it 
    - Cant use it     
    - Cant understand it 
- Elevate other developers
    - Help them focus on their business problems 
    - Automate away toil
    - Help them flow downhill to success    

![Empathy Developer](/images/devops-devsecops/04-empathy-developer.png "Empathy Developer Icon")

---
Reference: https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS180x+2T2022/home
