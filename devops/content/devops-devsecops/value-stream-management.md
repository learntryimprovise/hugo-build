+++
title = "Value Streams Management"
weight = 4
chapter = false
+++

---
- [Introduction](#introduction)
- [Mapping Value Streams](#mapping-value-streams)
- [Improving Team Value Streams](#improving-team-value-streams)
- [Improving Organizational Value Streams](#improving-organizational-value-streams)
---
### Introduction
- **Value = Impact - Total Cost of Ownership**
    - ***Impact*** means "How much it contributes to Goal" 
    - ***TCO*** means Cost to build maintain and operate
- Reducing waste reduces cost and  improves the value delivered
- Faster Feedback allows faster improvement of delivered value
- **Value Stream Management** is a disciplined process of reducing waste and improving flow to better deliver value to the end user and accelerate feedback

![Value Stream Management](/images/devops-devsecops/03-value-stream-management.png "Value Stream Management Icon")

![Value Flywheel](/images/devops-devsecops/03-value-flywheel.png "Value Flywheel Icon")

---
### Value Streams Mapping
- **Value stream mapping (VSM)** is a popular lean management process for visualizing all of the various steps in a workflow. This concept is a central part of [Toyota’s famous production model](https://tlmc.toyotauk.com/our-services/training/value-stream-mapping.html). It aims to avoid waste, maximize efficiency, and improve how workers bring products to market. 
- It is a highly versatile strategy that you can apply to just about any enterprise process or workflow. When it boils down to it, value stream mapping is outlining a process, breaking it down into smaller parts, and analyzing each component to determine its effectiveness. Remember that your business is unique. That being the case, it will have its own bespoke value stream maps. In other words, no two businesses will have the exact same workflows because they don’t have the exact same products.
- **The Benefits of Value Stream Mapping for DevOps** - Suffice it to say that value stream management can profoundly affect how your company approaches software production. With this in mind, here are some of the major benefits of VSM. 
    - ***Prioritize Customer Needs*** - Oftentimes, dev teams spend so much time and effort planning and iterating software that they lose track of what customers are looking for. It’s easy to prioritize requests from internal stakeholders but fall short in terms of meeting customers’ needs. 
    - By prioritizing the value stream mapping process, you can cut through the noise and focus on creating customer-centric software that aligns with what they are looking for. This becomes increasingly important when releasing changes and modifying live production environments.
    - ***Streamline Production*** - Value stream management can help iron out the kinks in your DevOps plan, leading to productivity gains. What’s more, it can also help you identify workflows that are holding teams back. Armed with that information, you can free engineers to focus on important tasks. 
    - ***Enable Automation*** - DevOps works best when there’s a high level of automation in the development pipeline. But to automate effectively, you need to understand your entire end-to-end processes, identify high-need areas, and determine exactly how changes will impact workflows. 
- Value stream mapping can help you visualize your production environment and identify areas that are ideal for automation. At the same time, you’ll be able to identify core processes that you absolutely shouldn’t automate. 

![Source: Lean Enterprise (O'Reilly) by Jez Humble, Joanne Molesky, and Barry O'Reilly, 2014](/images/devops-devsecops/03-devops-process-work-visibility-in-value-stream-9-stages.svg "Source: Lean Enterprise (O'Reilly) by Jez Humble, Joanne Molesky, and Barry O'Reilly, 2014 Icon")

- **How Value Stream Mapping Aligns with DevOps Culture** - While you can apply value stream mapping to just about any workflow, it aligns perfectly with DevOps culture. That makes it an excellent strategy for engineering leaders who want to increase visibility and build trust across their team and cultivate a stronger culture. Here’s why.
    - ***Integrate DevSecOps*** - Many DevOps teams are looking to “shift left” and integrate security teams via DevSecOps. This can be a big change—to the point, it requires forming a new strategy.  By building a value stream map and sharing it with your team, you can align participants and build a new DevSecOps strategy that works for everyone. Doing so will create an opportunity to identify barriers, optimize workflows, and look for collaboration opportunities.
    - ***Measure Performance*** - A big part of value stream mapping initiatives involves picking key performance indicators (KPIs). After that, you need to track them throughout your production cycles. Value stream KPIs can provide a way to measure DevOps and DevSecOps effectiveness, helping you optimize your team’s overall output.
    - Our research on the development pipeline has shown that cycle time, specifically pull request pickup time, is the most important indicator of development pipeline health. And this ties in with the next benefit: avoiding idle time.
    - ***Cycle time benchmarks*** - Avoid Time Waste. In the traditional approach to software delivery, it’s common for developers to request support from other teams and pass along work orders. But with DevOps, team members act with greater autonomy and take ownership of workflows and projects. With value stream mapping, you can explore how teams work with each other. You can also try to identify processes that waste time and drive up production costs. By doing so, you can implement process improvements that expedite workflows and prevent backlogs.
    - ***Improve Governance*** - Without full visibility into your production environment, it’s easy to lose track of the identities that have access to your systems and data. Unfortunately, once you lose track of your identities, it’s easier to fall victim to data breaches and permission escalation.

![Source: Lean Enterprise (O'Reilly) by Jez Humble, Joanne Molesky, and Barry O'Reilly, O'Reilly, 2014.](/images/devops-devsecops/03-devops-process-work-visibility-in-value-stream-stages-with-notes-and-timeline.svg "Source: Lean Enterprise (O'Reilly) by Jez Humble, Joanne Molesky, and Barry O'Reilly, O'Reilly, 2014. Icon")

- **Value Stream Mapping Best Practices** - Value stream mapping has the potential to change the way your team approaches software development. That said, there are many pitfalls to avoid. As such, you’ll want to keep some best practices in mind before starting on this journey. 
    - ***Explain Why Your Team Needs Value Stream Mapping*** - As with any process, it’s a good idea to explain why your team is moving forward with VSM before you start the exercise. This can help prevent pushback and give you a chance to answer questions and guide team members through the new system.
    - ***Respect Autonomy*** - You may receive some pushback from employees due to concerns about real-time visibility into daily workflows. Keep in mind that VSM isn’t about trying to control what people are doing. Instead, it provides a framework for autonomy. By acting with greater transparency, you will improve the flow to move faster and more efficiently. This, in turn, results in a faster, easier, and less stressful development environment.
    - ***Increase Transparency for Team Members*** - Value stream mapping can be a great learning resource for team members. If you build an effective plan, team members can have a deeper understanding of how software comes together and what their colleagues are working on. 
    - For this reason, you should strive to create a culture of transparency and encourage everyone to share insights with team members. Wherever possible, knock down silos and focus on bringing team members closer together.
- If you want to maximize value stream mapping and management, you need to have access to real-time visibility on your Git and project management data and a developer-first approach to improvement. It’s that simple.

---
### Improving Organizational Value Streams
- **Increasing risk through functional silos** Each handoff between teams adds a dependent value stream, additional wait time, and increased batch size.

![Organizational Value Stream](/images/devops-devsecops/03-organization-value-stream.png "Organizational Value Stream icon")

- In order to handle those functional silos based risk is to Remove hard dependencies

![Organizational remove hard dependencies](/images/devops-devsecops/03-remove-hard-dependencies.png "Organizational remove hard dependencies icon")

---
Reference: 
- https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS180x+2T2022/home
- https://cloud.google.com/architecture/devops/devops-process-work-visibility-in-value-stream
- https://linearb.io/blog/value-stream-mapping-devops/