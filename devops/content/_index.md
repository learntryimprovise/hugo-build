+++
title = "Home"
weight = 1
chapter = false
+++

### Info

{{% notice info %}}
This is an Informative Site meant to share learnings. Sharing is caring.
{{% /notice %}}

### Where to Start

{{% notice tip %}}
Left Vertical Menu + Search Box are the two best places to find your required content. 
{{% /notice %}}

### Code of Conduct

{{% notice warning %}}
If you see any content or example matching with other pages on Internet or Official documentations, Then we may have learned from those material. We are really Thankful for such 
pages on Internet or Official documentations. We try to refer the links on respective documentation that we are trying to create here as reference. 
{{% /notice %}}

### Contact Us

{{% notice note %}}
We are trying our best to get content updated here with accuracy. If you find any information missing or less accurate Please feel free to write to learntryimprovise@gmail.com
{{% /notice %}}