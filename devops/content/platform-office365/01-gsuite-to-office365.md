+++
title = "Gsuite to Office365 Migration - Overview"
slug = "office365"
tags = ["tutorials","gsuite","office","office365"]
weight = 19000
+++
### Overview

First we will talk about all steps that we need to get executed to get migration achieved.

#### Overall Flow of steps for migration

* Top Level Domain registration and account activation on Office365 ` topleveldomain.com `
* Generating final list of users from Gsuite (Google Workspace) for migration to Office 365

```
- user01@topleveldomain.com
- user02@topleveldomain.com
- user03@topleveldomain.com
```

* Second domain creation on Gsuite (Google Workspace) ` gsuite.topleveldomain.com `
* Adding Secondary Gsuite (Google Workspace) domain details to DNS 
* Addition of Secondary Gsuite (Google Workspace) domain to each user in Gsuite (Google Workspace)

```
- user01@gsuite.topleveldomain.com
- user02@gsuite.topleveldomain.com
- user03@gsuite.topleveldomain.com
```

* Service Account creation on Gsuite (Google Workspace) with required API access `gsuite_o365_migration_user`
* Second domain creation on Office365 ` o365.topleveldomain.com `
* Adding Secondary Office365 domain details to DNS
* Addition of Secondary Office365 domain to each user in Office365

```
- user01@o365.topleveldomain.com
- user02@o365.topleveldomain.com
- user03@o365.topleveldomain.com
```
* Based on generated list of users from Gsuite (Google Workspace) get users created in Office365
* Assign Microsoft Business License to each user
* Divide list of users into batches which can be migrated in phased manner
* Execute Batch wise migration on Office365
* Reset Password for each user in respective batch for Manual G-drive to One Drive data sync
* Enable MFA on Office365 Side using Mobile OTP based mechanism
* Complete migration for all batches one by one
* Post completion of all batches proceed with MX record changes to get traffic mapped to Office365
* After a week's duration proceed with deletion of accounts from Gsuite (Google Workspace)
* Remove Second domain created on Gsuite (Google Workspace)
* Remove Service account from Gsuite (Google Workspace)
* Remove Second domain alias added on to Office365 users
* Remove Second domain created on Office365
* Enable Self service on Office365
* Conclude your Migration from Gsuite (Google Workspace) to Office365
---
[Contact Us](/)
