+++
title = "Gitlab & Remote Foundations"
weight = 4003
chapter = false
+++

---
Here, you can expect to achieve the following:
- Demonstrate and learn the value of a bias for async communications.
- Practice strategies to implement async communication best practices into your day-to-day role at GitLab.
- Apply a strategy to design a remote workplace that favors async communication over synchronous, that reinforces our values.
---
### 1. The importance of a handbook-first approach to documentation
https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/#introduction
- A handbook-first approach to documentation is sneakily vital to a well-run business. While it feels skippable — inefficient, even — the outsized benefits of intentionally writing down and organizing process, culture, and solutions are staggering. Conversely, avoiding structured documentation is the best way to instill a low-level sense of chaos and confusion that hampers growth across the board.
---
##### Why does handbook-first documentation matter?
- In early-stage startups, it's particularly tempting to avoid a documentation strategy. With only a few team members, it's feasible to keep everyone informed via meetings, Slack, or email threads. Long-term, this oversight becomes increasingly harmful.
- As a team scales, the need for documentation increases in parallel with the cost of not doing it. Said another way, implementing a documentation strategy becomes more difficult — yet more vital — as a company ages and matures.
- Documentation is rarely placed on the same pedestal with metrics such as sales and client retention during a company's formative years, but it deserves to be. The difference between a well-documented company and one that eschews a handbook is stark.
- A handbook-first organization is home to team members who benefit from having a single source of truth to lean on. This type of organization is able to operate with almost supernatural efficiency. An organization that does not put concerted effort into structured documentation has no choice but to watch its team members ask and re-ask for the same bits of data in perpetuity, creating a torturous loop of interruptions, meetings, and suboptimal knowledge transfers.
- **Let your Slack or Teams or chat messages expire quickly**
- At GitLab, only 90 days of Slack activity is retained. After that, it's gone. This is intentional, as it prevents Slack as being useful as a tool for managing projects end-to-end. Slack, Microsoft Teams, and similar tools are instant messaging platforms, which may work to the detriment of a truly asynchronous culture.
- Leaders who are serious about ensuring that their team can rely on a single source of truth will be ruthless when it comes to instant message retention. If team members know that they can search their instant message history for updates on a given project, there is no motivation to document progress in a place that is universally accessible. This creates massive knowledge gaps and further splinters communication, alignment, and understanding throughout an organization.
- A limited retention policy acts as a forcing function. It nudges team members to discuss work matters in a location that is directly tied to the ultimate single source of truth. At GitLab, all work, process, and policies are documented in the handbook.
- To get there, discussions begin in GitLab Issues and/or Merge Requests — not in Slack. This ensures that whatever is merged into the handbook has a proper trail, full of context and universally accessible.
- **Documentation is never finished**
- Handbooks, and the documentation that creates them, are never finished. They are evolving, living entities, and it may take months or years to feel even remotely comprehensive.
- Resist the urge to abandon documentation plans when crisis hits. The most powerful documentation is that which is derived from failure. A lesson learned provides a vital roadmap of what to avoid, and how to operate better, in the future.
---
### 3. Adopting a self-service and self-learning mentality
---
### 4. Informal Communication in an all-remote environment
---
### 5. Embracing Asynchronous Communication
---
### 6. All-Remote Meetings
---
### 7. Communicating effectively and responsibly through text
---
### 8. Building and reinforcing a sustainable culture
---
### 9. Combating burnout, isolation, and anxiety in the remote workplace
---
### 10. How do you collaborate and whiteboard remotely?
---
### 11. The non-linear workday - re-imagining routine in an all-remote environment

---
### References : https://about.gitlab.com/handbook/values/