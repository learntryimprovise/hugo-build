+++
title = "VPC - Network ACL"
weight = 3
chapter = false
+++
---
- A **network access control list (ACL)** allows or denies specific inbound or outbound traffic at the subnet level. 
- Some Basics are,
    - Network ACL rules
    - Default network ACL
    - Custom network ACL
    - Ephemeral ports
    - Path MTU Discovery
    - Recommended rules for VPC scenarios
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html)