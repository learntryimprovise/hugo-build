+++
title = "VPC - Route Tables"
weight = 2
chapter = false
+++
---
- A route table contains a set of rules, called routes, that determine where network traffic from your subnet or gateway is directed.
- Some Basics,
    - Route table concepts
    - Subnet route tables
    - Gateway route tables
    - Route priority
    - Route table quotas
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Route_Tables.html)