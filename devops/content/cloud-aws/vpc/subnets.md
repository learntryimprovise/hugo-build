+++
title = "VPC - Subnets"
weight = 1
chapter = false
+++
---
- A **subnet** is a range of IP addresses in your VPC. You can launch AWS resources into your subnets.
- Some basics are,
    - Subnet sizing
    - Subnet routing
    - Subnet security
    - Work with subnets
    - Use subnet CIDR reservations
    - Group CIDR blocks using managed prefix lists
    - Configure route tables
    - Control traffic to subnets using Network ACLs
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/vpc/latest/userguide/configure-subnets.html)