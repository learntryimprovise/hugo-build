+++
title = "Service Quotas"
weight = 20
chapter = false
+++
---
- With **Service Quotas**, you can view and manage your quotas for AWS services from a central location. Quotas, also referred to as limits in AWS services, are the maximum values for the resources, actions, and items in your AWS account. 
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/servicequotas/latest/userguide/intro.html)
- [Requesting Quota Changes](https://docs.aws.amazon.com/servicequotas/latest/userguide/request-quota-increase.html)