+++
title = "Elastic Container Registry"
weight = 8
chapter = false
+++
---
- **Amazon Elastic Container Registry (Amazon ECR)** is an AWS managed container image registry service that is secure, scalable, and reliable.
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/AmazonECR/latest/userguide/what-is-ecr.html)