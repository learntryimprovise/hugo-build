+++
title = "Billing and Reports"
weight = 20
chapter = false
+++
---
- The [AWS Billing console](https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/billing-what-is.html) contains features to pay your AWS bills, organize and report your AWS cost and usage, and manage your consolidated billing if you're a part of AWS Organizations.
- The [AWS Cost Management console](https://docs.aws.amazon.com/cost-management/latest/userguide/what-is-costmanagement.html) has features that you can use for budgeting and forecasting costs and methods for you to optimize your pricing to reduce your overall AWS bill.
- The [AWS Cost and Usage Reports (AWS CUR)](https://docs.aws.amazon.com/cur/latest/userguide/what-is-cur.html) contains the most comprehensive set of cost and usage data available.