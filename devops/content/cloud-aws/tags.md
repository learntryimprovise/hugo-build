+++
title = "Tags"
weight = 2
chapter = false
+++
---
- A **tag** is a label that you assign to an AWS resource. Each tag consists of a key and an optional value, both of which you define.
- Tags enable you to categorize your AWS resources in different ways, for example, by purpose, owner, or environment. 
- Resource tags can be implemented to have attribute-based control (ABAC). IAM policies can be created to allow operations based on the tags for the resource.
- Tags can be used to organize your AWS bill to reflect your own cost structure.
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Tags.html)