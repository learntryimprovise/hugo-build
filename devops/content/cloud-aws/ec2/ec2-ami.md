+++
title = "EC2 - AMI"
weight = 1
chapter = false
+++
---
- An **Amazon Machine Image (AMI)** is a template that contains a software configuration (for example, an operating system, an application server, and applications). From an AMI, you launch an instance, which is a copy of the AMI running as a virtual server in the cloud. You can launch multiple instances of an AMI
- When you select an AMI, consider the following requirements you might have for the instances that you want to launch:
    - The Region
    - The operating system
    - The architecture: 32-bit (i386), 64-bit (x86_64), or 64-bit ARM (arm64)
    - The root device type: Amazon EBS or instance store
    - The provider (for example, Amazon Web Services)
    - Additional software (for example, SQL Server, AI/ML libraries)
---
### Reference : 
- [Finding an AMI](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html)
- [Create an AMI from an Amazon EC2 Instance](https://docs.aws.amazon.com/toolkit-for-visual-studio/latest/user-guide/tkv-create-ami-from-instance.html)
- [Create API using Packer](#)
- [Create API using Pulumi](#)