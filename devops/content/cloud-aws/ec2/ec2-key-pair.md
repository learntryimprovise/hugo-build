+++
title = "EC2 - Key Pair"
weight = 4
chapter = false
+++
---
- A **key pair**, consisting of a public key and a private key, is a set of security credentials that you use to prove your identity when connecting to an Amazon EC2 instance.
- Anyone who possesses your private key can connect to your instances, so it's important that you store your private key in a secure place.
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html)
- [I've lost my private key. How can I connect to my Linux instance?](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesConnecting.html#replacing-lost-key-pair)