+++
title = "Elastic Compute Cloud"
weight = 4
chapter = false
+++
---
- **Amazon Elastic Compute Cloud (Amazon EC2)** is a web service that provides resizable computing capacity—literally, servers in Amazon's data centers—that you use to build and host your software systems.
- Basics
    - Instance types
    - AMIs 
    - Regions 
    - Zones 
    - Tags 
- Networking and security
    - Key pairs 
    - Security groups 
    - Elastic IP addresses 
    - Virtual private clouds 
- Storage
    - Amazon EBS
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)
- [Tutorial: Installing LAMP stack on EC2 instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2022.html)
- [Tutorial: Configure SSL/TLS on EC2 instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/SSL-on-amazon-linux-2022.html)
- [Tutorial: Increase the size of an Amazon EBS volume on an EC2 instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/modify-ebs-volume-on-instance.html)
- [Tutorial: Host a WordPress blog on EC2 instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/hosting-wordpress-aml-2022.html)