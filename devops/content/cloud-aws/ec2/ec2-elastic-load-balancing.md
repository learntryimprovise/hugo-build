+++
title = "EC2 - Elastic Load Balancing (ELB)"
weight = 8
chapter = false
+++
---
- **Elastic Load Balancing** automatically distributes your incoming traffic across multiple targets, such as EC2 instances, containers, and IP addresses, in one or more Availability Zones. 
- Elastic Load Balancing monitors the health of its registered targets, and routes traffic only to the healthy targets. 
- Elastic Load Balancing scales your load balancer capacity automatically in response to changes in incoming traffic.
- Types are as below based on your implementation,
    - Application Load Balancers
    - Network Load Balancers
    - Gateway Load Balancers
    - Classic Load Balancers
- Target Groups do work on similar line.    
---
### Reference : 
- [Official Documentation](https://docs.aws.amazon.com/elasticloadbalancing/latest/userguide/what-is-load-balancing.html)