+++
title = "Configuring OS Monitoring"
weight = 12009
chapter = false
tags = ['prometheus','monitoring']
+++

##### [Previous](../c08-prometheus) 
---

Using Node Exporter
  - CPU
  - Memory 
  - Disk I/O
  - Open Files
  - Network I/O
  - GPU
  - Ping


---
##### [Next](../c10-prometheus)
