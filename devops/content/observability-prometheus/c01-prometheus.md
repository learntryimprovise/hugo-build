+++
title = "Gaining Basic Understanding"
weight = 12001
chapter = false
tags = ['prometheus','monitoring']
+++

##### [Previous](../)
---

Prometheus has become the most popular tool for monitoring Kubernetes workloads. The gap Prometheus fills is for monitoring and alerting.

Prometheus is the second project that graduates, after Kubernetes, from the [Cloud Native Computing Foundation (CNCF)](https://www.cncf.io/). Not many projects have been able to graduate yet.

Now, let’s talk about Prometheus from a more technical standpoint. Prometheus pulls metrics (key/value) and stores the data as time-series, allowing users to query data and alert in a real-time fashion. At given intervals, Prometheus will hit targets to collect metrics, aggregate data, show data, or even alert if some thresholds are met—in spite of not having the most beautiful GUI in the world.

Prometheus integrates workloads by pulling metrics (or scrapping metrics, as they call it), you have to instrument your applications properly.Prometheus can push metrics with a PushGateway. But keep in mind that the preferable way to collect data is to pull metrics from an application’s endpoint.

You can find more details in Prometheus documentation regarding how they recommend instrumenting your applications properly. 

We will understand more about [Time Series Databases](https://www.influxdata.com/time-series-database/) so that we can use Prometheus effectively.

---
##### [Next](../c02-prometheus)
