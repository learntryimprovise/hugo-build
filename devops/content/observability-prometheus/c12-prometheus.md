+++
title = "To Do"
weight = 12012
chapter = false
tags = ['prometheus','monitoring']
+++

##### [Previous](../c11-prometheus) 
---

Below are upcoming chapters, So stay tuned
##### Configuring Mariadb Monitoring
##### Configuring Postgresql Monitoring
##### Configuring Mariadb Monitoring
##### Configuring Nginx Monitoring
##### Configuring Apache Monitoring
##### Configuring Keepalived Monitoring
##### Configuring Haproxy Monitoring
##### Configuring Redis Monitoring
##### Configuring Jenkins Monitoring
##### Configuring Kafka Monitoring
##### Configuring Grafana Monitoring
##### Configuring Mayan EDMS Monitoring
##### Configuring Custom Go App Monitoring
##### Configuring Custom Java App Monitoring
##### Configuring Custom Python App Monitoring
##### Configuring Custom Node App Monitoring
##### Configuring Custom C/C++ App Monitoring
##### Configuring Custom Docker Swarm Cluster Monitoring
##### Configuring Custom Docker Kubernetes Cluster Monitoring
##### Configuring Container Monitoring
##### Configuring AWS RDS Monitoring
https://github.com/percona/rds_exporter

---
##### [Next](../)
