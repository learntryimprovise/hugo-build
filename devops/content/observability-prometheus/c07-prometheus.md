+++
title = "Configuring Rules in Prometheus"
weight = 12007
chapter = false
tags = ['prometheus','monitoring']
+++

##### [Previous](../c06-prometheus) 
---

  - 1 rule per page 
  - https://awesome-prometheus-alerts.grep.to/rules.html


---
##### [Next](../c08-prometheus)
