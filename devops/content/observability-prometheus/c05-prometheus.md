+++
title = "Installing Alert Manager"
weight = 12005
chapter = false
tags = ['prometheus','monitoring']
+++

##### [Previous](../c04-prometheus) 
---

  - configuring prometheus
  - configuring alertmanager
  - email alerts
  - slack alerts

---
##### [Next](../c06-prometheus)
