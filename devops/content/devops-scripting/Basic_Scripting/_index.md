+++
title = "Basic_Scripting"
weight = 3100
chapter = false
pre = "<b>1. </b>"
+++

- Purpose of Shell Script?
- Key Components in Shell Script
- Writing Shell Script
- Shebang
- Comment, Execute , Debug
- Environment Variable
- Keyboard Input
- Arithmetic Operation
- Pathname Expansion
- Alias, Startup
- Conditional Statement
- Loop, CLI Argument
- STDIN/STDOUT/STDERR
- Pipe, Function
