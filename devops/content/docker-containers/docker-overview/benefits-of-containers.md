+++
title = "Benefits of Containers"
weight = 8106
chapter = false
pre = "<b>1.6 </b>"
+++
---
### Benefits of containers
Containers are a streamlined way to build, test, deploy, and redeploy applications on multiple environments from a developer’s local laptop to an on-premises data center and even the cloud. Benefits of containers include:
- Less overhead - Containers require less system resources than traditional or hardware virtual machine environments because they don’t include operating system images.
- Increased portability - Applications running in containers can be deployed easily to multiple different operating systems and hardware platforms.
- More consistent operation - DevOps teams know applications in containers will run the same, regardless of where they are deployed.
- Greater efficiency - Containers allow applications to be more rapidly deployed, patched, or scaled.
- Better application development - Containers support agile and DevOps efforts to accelerate development, test, and production cycles.
### Container use cases
- "Lift and shift" existing applications into modern cloud architectures - Some organizations use containers to migrate existing applications into more modern environments. While this practice delivers some of the basic benefits of operating system virtualization, it does not offer the full benefits of a modular, container-based application architecture.
- Refactor existing applications for containers - Although refactoring is much more intensive than lift-and-shift migration, it enables the full benefits of a container environment.
- Develop new container-native applications - Much like refactoring, this approach unlocks the full benefits of containers.
- Provide better support for microservices architectures - Distributed applications and microservices can be more easily isolated, deployed, and scaled using individual container building blocks.
- Provide DevOps support for continuous integration and deployment (CI/CD) - Container technology supports streamlined build, test, and deployment from the same container images.
- Provide easier deployment of repetitive jobs and tasks - Containers are being deployed to support one or more similar processes, which often run in the background, such as ETL functions or batch jobs.

---
Proceed with next page in this Book [What is Docker](/docker-containers/docker-overview/what-is-docker)
