+++
title = "What is OCI"
weight = 8109
chapter = false
pre = "<b>1.9 </b>"
+++
---

### Open Container Initiative
- [Open Container Initiative (OCI)](https://opencontainers.org/) was established in 2015 by Docker and other industry leaders as a Linux Foundation project. It drives industry standards around container formats and runtime. They maintain two specifications at present: an Image Specification (image-spec) and a Runtime Specification (runtime-spec).
- Basically, an OCI implementation downloads an OCI image and then unpacks that image into an OCI runtime filesystem bundle. Then, that OCI runtime bundle can be run by an OCI runtime. There’s a standard reference implementation of OCI runtime known as runc that was originally developed by Docker. There are several other OCI compliant runtimes like Kata Containers.

### Cloud-Native Computing Foundation
- [Cloud-Native Computing Foundation (CNCF)](https://www.cncf.io/) was also established in 2015 as a Linux Foundation project with an aim to advance container technologies and align the industry around its evolution. 
- CNCF maintains a number of hosted sub-projects including containerd, Kubernetes, Prometheus, Envoy, CNI, and CoreDNS, to name a few.
- Here, containerd is a graduated CNCF project that aims to provide an industry-standard core container runtime available as a daemon for Linux and Windows. Container Network Interface (CNI) is an incubating CNCF project that was originally developed by CoreOS and consists of specifications and libraries for writing plugins to configure network interfaces.

---
Proceed with next page in this Book [Why OCI](/docker-containers/docker-overview/why-oci)