+++
title = "Key Components in Docker"
weight = 8201
chapter = false
pre = "<b>2.1 </b>"
+++

### Docker Image
![Docker Components Dockerfile](/images/docker-containers/docker-components-dockerfile-to-container.png)
![Docker Components Docker Images](/images/docker-containers/docker-components-docker-images.png)    
### Docker Registry
### Docker Network
### Docker Container
### Docker Volume
### Docker Compose
---
Proceed with next page in this Book [Overview of Docker](/docker-containers/docker-components/overview-of-docker)