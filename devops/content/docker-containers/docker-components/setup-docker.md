+++
title = "Setup Docker"
weight = 8205
chapter = false
pre = "<b>2.5 </b>"
+++

5. Setup Docker
    - What is Containerd?
    - Install Docker
    - Significance of Docker Programs
    - Manage Docker
    - Deep Dive into Docker

---
Proceed with next page in this Book [Configure Docker](/docker-containers/docker-components/configure-docker)
