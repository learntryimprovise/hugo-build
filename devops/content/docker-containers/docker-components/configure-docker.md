+++
title = "Configure Docker"
weight = 8206
chapter = false
pre = "<b>2.6 </b>"
+++

6. Configure Docker
    - Understanding Docker Configurations
    - Change Storage & Log Drivers of Docker
    - Apply Configuration Changes to Docker
    - Understanding Docker System Paths



---
Proceed with next page in this Book [Docker Compose](/docker-containers/docker-compose)
