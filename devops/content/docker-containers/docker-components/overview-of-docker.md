+++
title = "Overview of Docker"
weight = 8202
chapter = false
pre = "<b>2.2 </b>"
+++

2. Overview of Docker
    - Architecture of Docker-managed Containers
    - Docker Client
    - Docker Daemon
    - Docker Image
    - Docker Container
    - Docker Network
    - Docker Volume
    - Docker Registry

---
Proceed with next page in this Book [Container vs VM](/docker-containers/docker-components/container-vs-vm)