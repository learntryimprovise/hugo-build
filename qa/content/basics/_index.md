+++
title = "Basics of Testing"
weight = 1
chapter = false
+++

---
### [Functional Testing](functional-testing)

Functional testing is a type of software testing that validates the software system against the functional requirements/specifications. The purpose of Functional tests is to test each function of the software application by providing appropriate input and verifying the output against the Functional requirements.

- READ [What is Functional Testing? Types & Examples](https://www.guru99.com/functional-testing.html)
- READ [Functional Testing : A Detailed Guide](https://www.browserstack.com/guide/functional-testing)
---
### [Non-Functional Testing](nonfunctional-testing)

Non-functional testing is a type of software testing to test non-functional parameters such as reliability, load test, performance, and accountability of the software. The primary purpose of non-functional testing is to test the reading speed of the software system as per non-functional parameters. The parameters of non-functional testing are never tested before the functional testing.

- READ [What is Non Functional Testing](https://www.browserstack.com/guide/what-is-non-functional-testing)
- READ [Types of Non Functional Testing](https://www.guru99.com/non-functional-testing.html)
---
Reference: [https://roadmap.sh/qa](https://roadmap.sh/qa)