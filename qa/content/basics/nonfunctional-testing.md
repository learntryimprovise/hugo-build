+++
title = "Non Functional Testing"
weight = 2
chapter = false
+++

---
- [Load Testing](#load-testing)
- [Performance Testing](#performance-testing)
- [Stress Testing](#stress-testing)
- [Security Testing](#security-testing)
- [Accessibility Testing](#accessibility-testing)

---
### Load Testing

Load Testing is a type of Performance Testing that determines the performance of a system, software product, or software application under real-life-based load conditions. Load testing determines the behavior of the application when multiple users use it at the same time. It is the response of the system measured under varying load conditions.

- READ [Software Testing: Load Testing](https://www.geeksforgeeks.org/software-testing-load-testing/)
- READ [Load testing and Best Practices](https://loadninja.com/load-testing/)

---
### Performance Testing

- Performance Testing is a subset of Performance Engineering. It is a process of evaluating a system’s behavior under various extreme conditions. The main intent of performance testing is monitoring and improving key performance indicators such as response time, throughput, memory, CPU utilization, and more.
- There are three objectives (three S) of Performance testing to observe and evaluate: `Speed`, `Scalability`, and `Stability`.
- Types of Performance Testing, Following are the commonly used performance testing types, but not limited to:
    - Load Testing
    - Stress Testing
    - Spike Testing
    - Endurance Testing
    - Volume Testing
    - Scalability Testing
    - Capacity Testing

READ [Performance Testing Tutorial – Types (Example)](https://www.guru99.com/performance-testing.html)

---
### Stress Testing

**Stress Testing** is a type of **Performance Testing**. The objective of stress testing is to identify the breaking point of application under test under extreme normal load.

e.g. Injecting high volume of requests per second to an API might cause the disruption to its service, or throws HTTP 503 Service Unavailable or cause other consequences.

- READ [What is Stress Testing in Software Testing?](https://www.guru99.com/stress-testing-tutorial.html)

---
### Security Testing
Security Testing is a type of Software Testing that uncovers vulnerabilities, threats, or risks in a software application and prevents malicious attacks from intruders. The purpose of Security Tests is to identify all possible loopholes and weaknesses of the software system which might result in a loss of information, revenue, repute at the hands of the employees or outsiders of the organization.

- READ [What is Security Testing? Types with Example](https://www.guru99.com/what-is-security-testing.html)
- READ [Security Testing: Types, Tools, and Best Practices](https://www.guru99.com/what-is-security-testing.html)

---
### Accessibility Testing
Accessibility Testing is defined as a type of Software Testing performed to ensure that the application being tested is usable by people with disabilities like hearing, color blindness, old age, low vision and other disadvantaged groups.

- READ [What is Accessibility Testing? (Examples)](https://www.guru99.com/accessibility-testing.html)
- READ [Accessibility Testing Tutorial (Step By Step Guide)](https://www.softwaretestinghelp.com/what-is-web-accessibility-testing/)
- LIBRARY [IBM Accessibility Toolkit](https://www.ibm.com/able/)

---
Reference: [https://roadmap.sh/qa](https://roadmap.sh/qa)