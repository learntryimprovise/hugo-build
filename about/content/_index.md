+++
title = "Home"
weight = 1
chapter = false
+++
---
Nothing is more powerful than a **peaceful MIND**!

A connected mind is harder to break. Nothing goes out. Nothing comes in. Only peace and harmony resides inside. And no one can disturb such personality.

No matter what might have happened in past, one can just put aside everything and start all over from scratch. With such a start, nothing is impossible.

You define your own happiness. Your peace. Your joy. Your luck.

**Practice the following every day**:

> - **Step 1** : You don't need a fresh start. You need a disciplined mind!
> - **Step 2** : You don't need an approval. You need a self-respect soul!
> - **Step 3** : You don't need a diet. You need a healthy mind to a healthy body!

**Remember that:**

POWER is how you make up your mind!

It's how you rebuild your life! 

Every. Single. Day