+++
title = "Index & Page Weight Logic"
weight = 999997
chapter = false
pre = "<b></b>"
+++
---

| *Book*                                           | *Weight Value Range*   |
|:------------------------------------------------ |:---------------------- |
| **Book 01 - DevOps**                           |                        | 
| DevOps & Linux                                   | 1001                   |    
| DevOps & Windows                                 | 1002                   |    
| DevOps & Shell Scripting                         | 1003                   |    
| DevOps & SRE                                     | 1004                   |  
| DevOps & DevSecOps                               | 1005                   |  
| DevOps & DevTestOps                              | 1006                   |  
| **Book 02 - Cloud**                            |                        | 
| Cloud & AWS                                      | 2001                   |
| Cloud & GCP                                      | 2002                   |
| Cloud & DigitalOcean                             | 2003                   |
| Cloud & Azure                                    | 2004                   |
| Cloud & IBM                                      | 2005                   |
| **Book 03 - Docker**                           |                        | 
| Docker & Containers                              | 3001                   |
| Docker & Kubernetes                              | 3002                   |
| Docker & Swarm                                   | 3003                   |
| **Book04 - Git**                               |                        | 
| Git                                              | 4001                   |
| Gitlab Basics                                    | 4002                   |
| Gitlab & Remote Foundation                       | 4003                   |
| Gitlab & TeamOps                                 | 4004                   |
| BitBucket                                        | 4005                   |
| Github                                           | 4006                   |
| **Book 05 - CICD**                             |                        | 
| CICD & Jenkins                                   | 5001                   |             
| CICD & Gitlab CI                                 | 5002                   |             
| CICD & Github Actions                            | 5003                   |             
| CICD & Travis CI                                 | 5004                   |             
| CICD & Circle CI                                 | 5005                   |             
| CICD & BitBucket CI                              | 5006                   |             
| **Book 06 - IaaC**                             |                        | 
| IaaC & Ansible                                   | 6001                   |         
| IaaC & Chef                                      | 6002                   |         
| IaaC & Packer                                    | 6003                   |         
| IaaC & Puppet                                    | 6004                   |         
| IaaC & Terraform                                 | 6005                   |         
| IaaC & Vagrant                                   | 6006                   |         
| IaaC & Pulumi                                    | 6007                   |         
| **Book 07 - Observability**                    |                        | 
| Observability & ELK                              | 7001                   |  
| Observability & Fluentd                          | 7002                   |  
| Observability & Grafana                          | 7003                   |  
| Observability & Jaeger                           | 7004                   |  
| Observability & Logstash                         | 7005                   |  
| Observability & Loki                             | 7006                   |  
| Observability & Nagios                           | 7007                   |  
| Observability & Prometheus                       | 7008                   |  
| Observability & Sensu                            | 7009                   |  
| Observability & Splunk                           | 7010                   |  
| Observability & Zabbix                           | 7011                   |  
| Observability & DataDog                          | 7012                   |  
| **Book 08 - Platform**                         |                        | 
| Platform & office365                             | 8001                   |   

---
Proceed with next page in this Book[What should i Write about?](/want-to-contribute/what-should-i-write-about)