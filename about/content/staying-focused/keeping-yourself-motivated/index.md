+++
title = "Keeping Yourself Motivated"
weight = 4
chapter = false
+++

{{< gallery >}}

##### Source:
- [https://www.codeproject.com/Articles/5276331/Creating-an-Image-Gallery-with-Hugo-and-Lightbox2](https://www.codeproject.com/Articles/5276331/Creating-an-Image-Gallery-with-Hugo-and-Lightbox2)
- [https://twitter.com/art0fwork/status/1584478569652101120?s=12&t=I4L1EK6wgPfIRaki9KKibg](https://twitter.com/art0fwork/status/1584478569652101120?s=12&t=I4L1EK6wgPfIRaki9KKibg)
- [https://twitter.com/mind_essentials/status/1584236395119271936?s=12&t=HmQsQcpsO5ARIHZa7A2dGg](https://twitter.com/mind_essentials/status/1584236395119271936?s=12&t=HmQsQcpsO5ARIHZa7A2dGg)
- [https://twitter.com/livephilosophyy/status/1584120293836001283?s=12&t=HmQsQcpsO5ARIHZa7A2dGg](https://twitter.com/livephilosophyy/status/1584120293836001283?s=12&t=HmQsQcpsO5ARIHZa7A2dGg)
- [https://twitter.com/art0fwork/status/1583715596167876608?s=12&t=mGqg3e1vzZSOE_4ODMrJaA](https://twitter.com/art0fwork/status/1583715596167876608?s=12&t=mGqg3e1vzZSOE_4ODMrJaA)
- [https://twitter.com/mysteryofmind_/status/1581545223779278848?s=12&t=ESRxiiv26ofyEBeB4VzI6A](https://twitter.com/mysteryofmind_/status/1581545223779278848?s=12&t=ESRxiiv26ofyEBeB4VzI6A)
- [https://twitter.com/mind_essentials/status/1581594764499034112?s=12&t=ESRxiiv26ofyEBeB4VzI6A](https://twitter.com/mind_essentials/status/1581594764499034112?s=12&t=ESRxiiv26ofyEBeB4VzI6A)
- [https://twitter.com/motivateme247/status/1581317898878214145?s=12&t=YaWDl1GWniH2_fewkJ6kEA](https://twitter.com/motivateme247/status/1581317898878214145?s=12&t=YaWDl1GWniH2_fewkJ6kEA)